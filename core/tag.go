// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package core

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/i18n"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"github.com/goccy/go-json"
)

type TagSearchResult struct {
	Name            string `json:"tag"`
	AlternativeName string `json:"word"`
	Metadata        struct {
		Detail string      `json:"abstract"`
		Image  string      `json:"image"`
		Name   string      `json:"tag"`
		ID     json.Number `json:"id"`
	} `json:"pixpedia"`
	CoverArtwork Illust // Custom field to store extended info about the cover artwork
}

type ArtworkSearchResult struct {
	Artworks ArtworkResults
	Popular  struct {
		Permanent []ArtworkBrief `json:"permanent"`
		Recent    []ArtworkBrief `json:"recent"`
	} `json:"popular"`
	RelatedTags []Tag `json:"relatedTags"`
}

// TODO: embed anonymously inside ArtworkSearchResult or come up with a better name for this type
type ArtworkResults struct {
	Artworks []ArtworkBrief `json:"data"`
	Total    int            `json:"total"`
	LastPage int            `json:"lastPage"`
}

type ArtworkSearchSettings struct {
	Name     string // Keywords to search for
	Word     string // Mirror of the Name field, required for multiple keywords to work
	Category string // Filter by type, could be illustrations or manga
	Order    string // Sort by date
	Mode     string // Safe, R18 or both
	Ratio    string // Landscape, portrait, or squared
	Page     string // Page number
	Smode    string // Exact match, partial match, or match with title
	Wlt      string // Minimum image width
	Wgt      string // Maximum image width
	Hlt      string // Minimum image height
	Hgt      string // Maximum image height
	Tool     string // Filter by production tools (e.g. Photoshop)
	Scd      string // After this date
	Ecd      string // Before this date
}

func (s ArtworkSearchSettings) ReturnMap() map[string]string {
	return map[string]string{
		"Name":     s.Name,
		"Word":     s.Name,
		"Category": s.Category,
		"Order":    s.Order,
		"Mode":     s.Mode,
		"Ratio":    s.Ratio,
		"Smode":    s.Smode,
		"Wlt":      s.Wlt,
		"Wgt":      s.Wgt,
		"Hlt":      s.Hlt,
		"Hgt":      s.Hgt,
		"Scd":      s.Scd,
		"Ecd":      s.Ecd,
		"Tool":     s.Tool,
		"Page":     s.Page,
	}
}

func GetTagData(r *http.Request, name string) (TagSearchResult, error) {
	var tag TagSearchResult

	url := GetTagDetailURL(name)

	cookies := map[string]string{
		"PHPSESSID": session.GetUserToken(r),
	}

	rawResp, err := requests.FetchJSONBodyField(r.Context(), url, cookies, r.Header)
	if err != nil {
		return tag, err
	}

	rawResp, err = session.ProxyContentURLs(r, rawResp)
	if err != nil {
		return tag, err
	}

	err = json.Unmarshal(rawResp, &tag)
	if err != nil {
		return tag, err
	}

	return tag, nil
}

// GetSearch delegates the search operation to either getPopularSearch or getStandardSearch based on settings.Order.
func GetSearch(r *http.Request, settings ArtworkSearchSettings) (*ArtworkSearchResult, error) {
	if strings.ToLower(settings.Order) == "popular" {
		return getPopularSearch(r, settings)
	}
	return getStandardSearch(r, settings)
}

// getStandardSearch handles the standard search logic.
func getStandardSearch(r *http.Request, settings ArtworkSearchSettings) (*ArtworkSearchResult, error) {
	url := GetArtworkSearchURL(settings.ReturnMap())

	// p_ab := session.GetCookie(r, session.Cookie_P_AB)
	// if p_ab == "" {
	p_ab := config.GlobalConfig.GetP_AB()
	//}

	cookies := map[string]string{
		"PHPSESSID": session.GetUserToken(r),
		"p_ab_d_id": p_ab,
		"p_ab_id":   "8",
		"p_ab_id_2": "4",
	}

	resp, err := requests.FetchJSONBodyField(r.Context(), url, cookies, r.Header)
	if err != nil {
		return nil, err
	}

	resp, err = session.ProxyContentURLs(r, resp)
	if err != nil {
		return nil, err
	}

	// Replace "illust", "manga", and "illustManga" with "works" in the JSON response
	//
	// VnPower: IDK how to do better than this lol
	temp := bytes.ReplaceAll(resp, []byte(`"illust"`), []byte(`"works"`))
	temp = bytes.ReplaceAll(temp, []byte(`"manga"`), []byte(`"works"`))
	temp = bytes.ReplaceAll(temp, []byte(`"illustManga"`), []byte(`"works"`))

	// Define an intermediate struct to unmarshal the JSON response
	var resultRaw struct {
		ArtworksRaw json.RawMessage `json:"works"`
		Popular     struct {
			Permanent []ArtworkBrief `json:"permanent"`
			Recent    []ArtworkBrief `json:"recent"`
		} `json:"popular"`
		RelatedTags     []string              `json:"relatedTags"`
		TagTranslations TagTranslationWrapper `json:"tagTranslation"`
	}

	var artworks ArtworkResults
	var result ArtworkSearchResult

	// Unmarshal the modified JSON response into the intermediate struct
	err = json.Unmarshal(temp, &resultRaw)
	if err != nil {
		return nil, err
	}

	// Unmarshal the artworks data
	err = json.Unmarshal(resultRaw.ArtworksRaw, &artworks)
	if err != nil {
		return nil, err
	}

	// Populate thumbnails for each artwork
	for id, artwork := range artworks.Artworks {
		if err := artwork.PopulateThumbnails(); err != nil {
			audit.GlobalAuditor.Logger.Errorf("Failed to populate thumbnails for artwork ID %d: %v", id, err)
			return nil, fmt.Errorf("failed to populate thumbnails for artwork ID %d: %w", id, err)
		}
		artworks.Artworks[id] = artwork
	}

	for id, artwork := range resultRaw.Popular.Permanent {
		if err := artwork.PopulateThumbnails(); err != nil {
			audit.GlobalAuditor.Logger.Errorf("Failed to populate thumbnails for artwork ID %d: %v", id, err)
			return nil, fmt.Errorf("failed to populate thumbnails for artwork ID %d: %w", id, err)
		}
		resultRaw.Popular.Permanent[id] = artwork
	}

	for id, artwork := range resultRaw.Popular.Recent {
		if err := artwork.PopulateThumbnails(); err != nil {
			audit.GlobalAuditor.Logger.Errorf("Failed to populate thumbnails for artwork ID %d: %v", id, err)
			return nil, fmt.Errorf("failed to populate thumbnails for artwork ID %d: %w", id, err)
		}
		resultRaw.Popular.Recent[id] = artwork
	}

	// Assign artworks and popular data to the result
	result.Artworks = artworks
	result.Popular = resultRaw.Popular

	// Convert tag translations to Tag objects
	result.RelatedTags = TagTranslationsToTags(resultRaw.RelatedTags, resultRaw.TagTranslations)

	return &result, nil
}

// getPopularSearch handles the popular search logic.
func getPopularSearch(r *http.Request, settings ArtworkSearchSettings) (*ArtworkSearchResult, error) {
	// Check if popular search is enabled
	if !config.GlobalConfig.Feature.PopularSearchEnabled {
		return nil, i18n.Errorf("Popular search is disabled by server configuration.")
	}

	// Perform popular search
	searchArtworks, err := searchPopular(r.Context(), r, settings)
	if err != nil {
		return nil, err
	}

	// Create SearchResult
	result := &ArtworkSearchResult{
		Artworks: searchArtworks,
		// TODO: populate Popular (the regular one) and RelatedTags
	}

	// Populate thumbnails for each artwork
	for id, artwork := range result.Artworks.Artworks {
		if err := artwork.PopulateThumbnails(); err != nil {
			audit.GlobalAuditor.Logger.Errorf("Failed to populate thumbnails for artwork ID %d: %v", id, err)
			return nil, fmt.Errorf("failed to populate thumbnails for artwork ID %d: %w", id, err)
		}
		result.Artworks.Artworks[id] = artwork
	}

	return result, nil
}
