// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package core

import (
	"fmt"
	"html"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"github.com/PuerkitoBio/goquery"
)

// PixivDatetimeLayout defines the date format used by Pixiv
const PixivDatetimeLayout = "2006-01-02"

// PixivisionArticle represents a single article on Pixivision
type PixivisionArticle struct {
	ID          string
	Title       string
	Description []string
	Category    string
	Thumbnail   string
	Date        time.Time
	Items       []PixivisionArticleItem
	Tags        []PixivisionEmbedTag
}

// PixivisionEmbedTag represents a tag associated with a Pixivision article
type PixivisionEmbedTag struct {
	ID   string
	Name string
}

// PixivisionArticleItem represents an item (artwork) within a Pixivision article
type PixivisionArticleItem struct {
	Username string
	UserID   string
	Title    string
	ID       string
	Avatar   string
	Image    string
}

// PixivisionTag represents a tag page on Pixivision
type PixivisionTag struct {
	Title       string
	Description string
	Articles    []PixivisionArticle
	Total       int // The total number of articles
}

// PixivisionCategory represents a category page on Pixivision
type PixivisionCategory struct {
	Articles    []PixivisionArticle
	Title       string
	Description string
}

// PixivisionGetHomepage fetches and parses the Pixivision homepage
func PixivisionGetHomepage(r *http.Request, page string, lang ...string) ([]PixivisionArticle, error) {
	var articles []PixivisionArticle

	URL := generatePixivisionURL(fmt.Sprintf("?p=%s", page), lang)

	userLang := determineUserLang(URL, lang...)

	cookies := map[string]string{
		"user_lang": userLang,
	}

	resp, err := requests.PerformGETReader(r.Context(), URL, cookies, r.Header)
	if err != nil {
		return nil, err
	}

	// if resp.StatusCode == 404 {
	// 	return articles, i18n.Error("We couldn't find the page you're looking for")
	// }

	doc, err := goquery.NewDocumentFromReader(resp)
	if err != nil {
		return nil, err
	}

	// Parse each article on the homepage
	doc.Find("article.spotlight").Each(func(i int, s *goquery.Selection) {
		var article PixivisionArticle

		date := s.Find("time._date").AttrOr("datetime", "")

		article.ID = s.Find(`a[data-gtm-action=ClickTitle]`).AttrOr("data-gtm-label", "")
		article.Title = s.Find(`a[data-gtm-action=ClickTitle]`).Text()
		article.Category = s.Find("._category-label").Text()
		article.Thumbnail = parseBackgroundImage(s.Find("._thumbnail").AttrOr("style", ""))
		article.Date, _ = time.Parse(PixivDatetimeLayout, date)

		// Parse tags associated with the article
		s.Find("._tag-list a").Each(func(i int, t *goquery.Selection) {
			var tag PixivisionEmbedTag
			tag.ID = parseIDFromPixivLink(t.AttrOr("href", ""))
			tag.Name = t.AttrOr("data-gtm-label", "")

			article.Tags = append(article.Tags, tag)
		})

		articles = append(articles, article)
	})

	// Proxy the Thumbnail URL for each article
	for i := range articles {
		proxied, err := session.ProxyContentURLsNoEscape(r, []byte(articles[i].Thumbnail))
		if err != nil {
			return nil, err
		}
		articles[i].Thumbnail = string(proxied)
	}

	return articles, nil
}

// PixivisionGetTag fetches and parses a tag page on Pixivision
func PixivisionGetTag(r *http.Request, id string, page string, lang ...string) (PixivisionTag, error) {
	var tag PixivisionTag

	URL := generatePixivisionURL(fmt.Sprintf("t/%s/?p=%s", id, page), lang)

	userLang := determineUserLang(URL, lang...)

	cookies := map[string]string{
		"user_lang": userLang,
	}

	resp, err := requests.PerformGETReader(r.Context(), URL, cookies, r.Header)
	if err != nil {
		return tag, err
	}

	doc, err := goquery.NewDocumentFromReader(resp)
	if err != nil {
		return tag, err
	}

	tag.Title = doc.Find(".tdc__header h1").Text()

	// Extract and process the description
	fullDescription := doc.Find(".tdc__description").Text()
	parts := strings.Split(fullDescription, "pixivision") // split on the "pixivision currently has ..." boilerplate
	tag.Description = strings.TrimSpace(parts[0])

	// Extract total number of articles if available
	if len(parts) > 1 {
		re := regexp.MustCompile(`(\d+)\s+article\(s\)`)
		matches := re.FindStringSubmatch(parts[1])
		if len(matches) > 1 {
			tag.Total, _ = strconv.Atoi(matches[1])
		}
	}

	// Parse each article in the tag page
	doc.Find("._article-card").Each(func(i int, s *goquery.Selection) {
		var article PixivisionArticle

		// article.ID = s.Find(".arc__title a").AttrOr("data-gtm-label", "")
		// article.Title = s.Find(".arc__title a").Text()

		article.ID = s.Find(`a[data-gtm-action="ClickTitle"]`).AttrOr("data-gtm-label", "")
		article.Title = s.Find(`a[data-gtm-action="ClickTitle"]`).Text()
		article.Category = s.Find(".arc__thumbnail-label").Text()
		article.Thumbnail = parseBackgroundImage(s.Find("._thumbnail").AttrOr("style", ""))

		date := s.Find("time._date").AttrOr("datetime", "")
		article.Date, _ = time.Parse(PixivDatetimeLayout, date)

		// Parse tags associated with the article
		s.Find("._tag-list a").Each(func(i int, t *goquery.Selection) {
			var ttag PixivisionEmbedTag
			ttag.ID = parseIDFromPixivLink(t.AttrOr("href", ""))
			ttag.Name = t.AttrOr("data-gtm-label", "")

			article.Tags = append(article.Tags, ttag)
		})

		// Proxy the thumbnail URL for the article
		proxied, err := session.ProxyContentURLsNoEscape(r, []byte(article.Thumbnail))
		if err != nil {
			// if error occurs, we can leave the thumbnail unchanged or handle error as needed
			// Here, we choose to propagate the error
			tag.Articles = nil
			tag.Description = ""
			tag.Title = ""
			return
		}
		article.Thumbnail = string(proxied)

		tag.Articles = append(tag.Articles, article)
	})

	return tag, nil
}

// PixivisionGetArticle fetches and parses a single article on Pixivision
func PixivisionGetArticle(r *http.Request, id string, lang ...string) (PixivisionArticle, error) {
	var article PixivisionArticle

	URL := generatePixivisionURL(fmt.Sprintf("a/%s", id), lang)

	userLang := determineUserLang(URL, lang...)

	cookies := map[string]string{
		"user_lang": userLang,
	}

	resp, err := requests.PerformGETReader(r.Context(), URL, cookies, r.Header)
	if err != nil {
		return PixivisionArticle{}, err
	}

	doc, err := goquery.NewDocumentFromReader(resp)
	if err != nil {
		return PixivisionArticle{}, err
	}

	// Parse article metadata
	date := doc.Find("time._date").AttrOr("datetime", "")
	article.Title = doc.Find("h1.am__title").Text()
	article.Category = doc.Find(".am__categoty-pr ._category-label").Text()
	article.Thumbnail = strings.ReplaceAll(doc.Find(".aie__image").AttrOr("src", ""),
		"https://embed.pixiv.net", "/proxy/embed.pixiv.net")
	article.Date, _ = time.Parse(PixivDatetimeLayout, date)

	// Proxy the main thumbnail URL
	proxiedThumb, err := session.ProxyContentURLsNoEscape(r, []byte(article.Thumbnail))
	if err != nil {
		return PixivisionArticle{}, err
	}
	article.Thumbnail = string(proxiedThumb)

	// Parse article description
	doc.Find(".fab__paragraph p").Each(func(i int, s *goquery.Selection) {
		desc, err := s.Html()
		if err != nil {
			return
		}

		if desc == "<br/>" {
			return
		}

		desc = html.EscapeString(desc)

		article.Description = append(article.Description, desc)
	})

	// Parse artworks featured in the article
	doc.Find("._feature-article-body__pixiv_illust").Each(func(i int, s *goquery.Selection) {
		var item PixivisionArticleItem

		item.Title = s.Find(".am__work__title a.inner-link").Text()
		item.Username = s.Find(".am__work__user-name a.inner-link").Text()
		item.ID = parseIDFromPixivLink(s.Find(".am__work__title a.inner-link").AttrOr("href", ""))
		item.UserID = parseIDFromPixivLink(s.Find(".am__work__user-name a.inner-link").AttrOr("href", ""))
		item.Avatar = s.Find(".am__work__user-icon-container img.am__work__uesr-icon").AttrOr("src", "")
		item.Image = s.Find("img.am__work__illust").AttrOr("src", "")

		// Proxy the image URL
		proxiedImage, err := session.ProxyContentURLsNoEscape(r, []byte(item.Image))
		if err == nil {
			item.Image = string(proxiedImage)
		}
		// Proxy the avatar URL
		proxiedAvatar, err := session.ProxyContentURLsNoEscape(r, []byte(item.Avatar))
		if err == nil {
			item.Avatar = string(proxiedAvatar)
		}

		article.Items = append(article.Items, item)
	})

	// Parse tags associated with the article
	doc.Find("._tag-list a").Each(func(i int, s *goquery.Selection) {
		var tag PixivisionEmbedTag
		tag.ID = parseIDFromPixivLink(s.AttrOr("href", ""))
		tag.Name = s.AttrOr("data-gtm-label", "")

		article.Tags = append(article.Tags, tag)
	})

	return article, nil
}

// PixivisionGetCategory fetches and parses a category page on Pixivision
func PixivisionGetCategory(r *http.Request, id string, page string, lang ...string) (PixivisionCategory, error) {
	var category PixivisionCategory

	URL := generatePixivisionURL(fmt.Sprintf("c/%s/?p=%s", id, page), lang)

	userLang := determineUserLang(URL, lang...)

	cookies := map[string]string{
		"user_lang": userLang,
	}

	resp, err := requests.PerformGETReader(r.Context(), URL, cookies, r.Header)
	if err != nil {
		return PixivisionCategory{}, err
	}

	doc, err := goquery.NewDocumentFromReader(resp)
	if err != nil {
		return PixivisionCategory{}, err
	}

	category.Title = doc.Find(".ssc__name").Text()
	category.Description = doc.Find(".ssc__descriotion").Text() // NOTE: This is a typo in the original HTML

	// Parse each article in the category page
	doc.Find("._article-card").Each(func(i int, s *goquery.Selection) {
		var article PixivisionArticle

		// article.ID = s.Find(".arc__title a").AttrOr("data-gtm-label", "")
		// article.Title = s.Find(".arc__title a").Text()

		article.ID = s.Find(`a[data-gtm-action="ClickTitle"]`).AttrOr("data-gtm-label", "")
		article.Title = s.Find(`a[data-gtm-action="ClickTitle"]`).Text()
		article.Category = s.Find(".arc__thumbnail-label").Text()
		article.Thumbnail = parseBackgroundImage(s.Find("._thumbnail").AttrOr("style", ""))

		date := s.Find("time._date").AttrOr("datetime", "")
		article.Date, _ = time.Parse(PixivDatetimeLayout, date)

		// Proxy the thumbnail URL for the article
		proxied, err := session.ProxyContentURLsNoEscape(r, []byte(article.Thumbnail))
		if err != nil {
			// propagate error if proxying fails
			return
		}
		article.Thumbnail = string(proxied)

		category.Articles = append(category.Articles, article)
	})

	return category, nil
}

// generatePixivisionURL creates a URL for Pixivision based on the route and language
func generatePixivisionURL(route string, lang []string) string {
	template := "https://www.pixivision.net/%s/%s"
	language := "en" // Default
	availableLangs := []string{"en", "zh", "ja", "zh-tw", "ko"}

	// Validate and set the language if provided
	if len(lang) > 0 {
		t := lang[0]

		for _, i := range availableLangs {
			if t == i {
				language = t
			}
		}
	}

	return fmt.Sprintf(template, language, route)
}

// re_lang is a regular expression to extract the language code from a URL
var re_lang = regexp.MustCompile(`.*\/\/.*?\/(.*?)\/`)

// determineUserLang determines the language to use for the user_lang cookie
func determineUserLang(url string, lang ...string) string {
	// Check if language is provided in parameters
	if len(lang) > 0 && lang[0] != "" {
		return lang[0]
	}

	// Try to extract language from URL
	matches := re_lang.FindStringSubmatch(url)
	if len(matches) > 1 {
		return matches[1]
	}

	// Fallback to default language
	return "en"
}

// re_findid is a regular expression to extract the ID from a Pixiv link
var re_findid = regexp.MustCompile(`.*\/(\d+)`)

// parseIDFromPixivLink extracts the numeric ID from a Pixiv URL
func parseIDFromPixivLink(link string) string {
	return re_findid.FindStringSubmatch(link)[1]
}

// r_img is a regular expression to extract the image URL from a CSS background-image property
var r_img = regexp.MustCompile(`background-image:\s*url\(([^)]+)\)`)

// parseBackgroundImage extracts the image URL from a CSS background-image property
func parseBackgroundImage(link string) string {
	matches := r_img.FindStringSubmatch(link)
	if len(matches) < 2 {
		return ""
	}
	return matches[1]
}
