// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package core

import (
	"fmt"
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"github.com/goccy/go-json"
)

func GetNewestFromFollowing(r *http.Request, contentType, mode, page string) (NewestFromFollowingResponse, error) {
	var resp NewestFromFollowingResponse

	url := GetNewestFromFollowingURL(contentType, mode, page)

	cookies := map[string]string{
		"PHPSESSID": session.GetUserToken(r),
	}

	rawResp, err := requests.FetchJSON(r.Context(), url, cookies, r.Header)
	if err != nil {
		return NewestFromFollowingResponse{}, err
	}

	rawResp, err = session.ProxyContentURLs(r, rawResp)
	if err != nil {
		return NewestFromFollowingResponse{}, err
	}

	err = json.Unmarshal(rawResp, &resp)
	if err != nil {
		return NewestFromFollowingResponse{}, err
	}

	// Convert tag translations to Tag objects
	resp.Body.Page.Tags = TagTranslationsToTags(nil, resp.Body.TagTranslation)

	// Populate thumbnails for each artwork
	for i, artwork := range resp.Body.Thumbnails.Illust {
		if err := artwork.PopulateThumbnails(); err != nil {
			audit.GlobalAuditor.Logger.Errorf("Failed to populate thumbnails for artwork ID %s: %v", artwork.ID, err)
			return NewestFromFollowingResponse{},
				fmt.Errorf("failed to populate thumbnails for artwork ID %s: %w", artwork.ID, err)
		}
		resp.Body.Thumbnails.Illust[i] = artwork
	}

	return resp, nil
}
