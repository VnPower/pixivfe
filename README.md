<h1 align="center">
  <img src="https://codeberg.org/repo-avatars/b9a8c82b56a5f8f466f3731164f71ed961ca971df89c6a1ceac618e3b5062050" alt="PixivFE logo" width="150" />
  <br />
  PixivFE
  <br />
  <a href="https://gitlab.com/pixivfe/PixivFE/-/commits/tailwind-rewrite"><img alt="Pipeline status on GitLab" src="https://gitlab.com/pixivfe/PixivFE/badges/tailwind-rewrite/pipeline.svg" /></a>
  <a href="https://crowdin.com/project/pixivfe" rel="nofollow"><img src="https://badges.crowdin.net/pixivfe/localized.svg" alt="Localization percentage on Crowdin" /></a>
</h1>

<!-- TODO: there must be a better solution than this -->
<p align="center">
  <b>
    <a href="https://codeberg.org/PixivFE/PixivFE/src/branch/tailwind-rewrite/README.md">English</a> |
    <a href="https://codeberg.org/PixivFE/PixivFE/src/branch/tailwind-rewrite/README.zh-CN.md">简体中文</a>
  </b>
</p>

PixivFE (lit. _Pixiv FrontEnd_) is a open-source, self-hostable alternative front-end for [pixiv](https://en.wikipedia.org/wiki/Pixiv).

Try it out right now with our [official public instance](https://pixiv.perennialte.ch/). Alternatively, see the [list of public instances](https://pixivfe-docs.pages.dev/instance-list/).

Or read our [documentation](https://pixivfe-docs.pages.dev/) for installation guides and more information. Read our [roadmap](https://pixivfe-docs.pages.dev/dev/roadmap/) and [scope](https://pixivfe-docs.pages.dev/dev/scope/) as well.

## Why use PixivFE?

- PixivFE lets you browse pixiv **anonymously** with all restrictions **removed**. No pixiv account is needed to access content.
- PixivFE prevents direct interaction with pixiv and its third-party services/analyzers. All processing **occurs server-side**, leaving only PixivFE client-side. No more Google Analytics tracking for every action!
- PixivFE follows the design philosophy of [**progressive enhancement**](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement), providing a core experience that works without JavaScript while adding features like asynchronous loading for quicker page navigations and user interactions that don't require the whole page to refresh when JavaScript is enabled. Our lightweight, modern interface minimizes browsing disruptions - unlike pixiv's original frontend.
- PixivFE is **free software** with fully open-source code and transparent development. Anyone can contribute, modify, and use it.

PixivFE prioritizes accessibility, privacy, and freedom. If you value these principles, [try PixivFE now](https://pixivfe-docs.pages.dev/instance-list/) - or better yet, [host it locally](https://pixivfe-docs.pages.dev/hosting/)!

## What PixivFE is not

- A product developed by _pixiv_
- A content scraping machine - don’t even try
- A perfect pixiv client - a lot of features are missing, check our [roadmap](https://pixivfe-docs.pages.dev/dev/roadmap/) and [scope](https://pixivfe-docs.pages.dev/dev/scope/)

## Quick start

You can get PixivFE up and running with [Docker](https://pixivfe-docs.pages.dev/hosting/hosting-pixivfe/#docker) or by [running from source](https://pixivfe-docs.pages.dev/hosting/hosting-pixivfe/#binary).

## Development

Use our build tool: `./build.sh help`.

Here are the build prerequisites. You may only install some of them.

- [Go 1.24 or higher](https://go.dev/doc/install)
- [Tailwind CSS CLI](https://github.com/tailwindlabs/tailwindcss/releases/latest)
- [jq](https://jqlang.github.io/jq/) (optional, to build i18n files)
- [semgrep](https://semgrep.dev/) (optional, to build i18n files)
- [Crowdin CLI](./doc/dev/features/i18n.md) (optional, to build i18n files)

To install Tailwind CSS CLI:

```bash
curl -sLO https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-x64
chmod +x tailwindcss-linux-x64
sudo mv tailwindcss-linux-x64 ~/.local/bin/tailwindcss
```

Then, to run the project:

```bash
# Clone the PixivFE repository
git clone https://codeberg.org/PixivFE/PixivFE.git && cd PixivFE

# Run PixivFE in development mode (templates will reload automatically)
PIXIVFE_DEV=true <other_environment_variables> ./build.sh run

# In a separate terminal, run Tailwind CSS in watch mode
tailwindcss -i assets/css/tailwind-style_source.css -o assets/css/tailwind-style.css --watch
```

**Note:** The project is hosted on two repositories that stay synchronized:

- [Codeberg](https://codeberg.org/PixivFE/PixivFE) is our official repository where all issues and pull requests should be submitted
- [GitLab](https://gitlab.com/pixivfe/PixivFE) is used for running our CI/CD pipelines

Any commits pushed to either repository will automatically synchronize with the other.

## Getting help

To get support, questions, and provide feedback, bug reports:

- Join our [Matrix room](https://matrix.to/#/#pixivfe:4d2.org)
- [Issue tracker](https://codeberg.org/PixivFE/PixivFE/issues) for bug reports
- Contact [VnPower](https://loang.net/~vnpower/me#contact)

## License

PixivFE is free software and is licensed under the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html).
