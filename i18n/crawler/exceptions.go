// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

/*
This file contains a map of strings that should be ignored by the i18n crawler,
such as empty strings, symbols, and template syntax that should remain unchanged
across all locales.
*/
package main

var IgnoreTheseStrings = map[string]bool{
	"":                      true,
	"»":                     true,
	"▶":                     true,
	"⧉ {{ .Pages }}":        true,
	"PixivFE":               true,
	"pixiv.net/i/{{ .ID }}": true,
}
