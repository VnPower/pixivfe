// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/config"

	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/i18n"
	"codeberg.org/vnpower/pixivfe/v2/server/middleware"
	"codeberg.org/vnpower/pixivfe/v2/server/middleware/limiter"
	"codeberg.org/vnpower/pixivfe/v2/server/proxychecker"
	"codeberg.org/vnpower/pixivfe/v2/server/router"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

func main() {
	if err := config.GlobalConfig.LoadConfig(); err != nil {
		panic(fmt.Errorf("failed to load configuration: %w", err))
	}

	// No err handling needed as audit.Init will panic if initialization fails
	audit.Init(&config.GlobalConfig)
	audit.GlobalAuditor.Logger.Info("Auditor initialized")

	i18n.Init()

	// Passing GenerateLinkToken() to package template here
	// avoids having to import package limiter
	template.Init(
		config.GlobalConfig.Development.InDevelopment,
		"assets/views",
		limiter.GetOrCreateLinkToken,
	)

	// Conditionally initialize and start the proxy checker
	if config.GlobalConfig.ProxyChecker.Enabled {
		go func() {
			var wgFirstCheck sync.WaitGroup
			wgFirstCheck.Add(1)

			startTime := time.Now() // Record the start time for logging

			go func() {
				ctx, cancel := context.WithTimeout(context.Background(), config.GlobalConfig.ProxyChecker.Timeout)
				proxychecker.CheckProxies(ctx)
				cancel()
				wgFirstCheck.Done()
				if config.GlobalConfig.ProxyChecker.Interval != 0 {
					for {
						time.Sleep(config.GlobalConfig.ProxyChecker.Interval)
						ctx, cancel := context.WithTimeout(context.Background(), config.GlobalConfig.ProxyChecker.Timeout)
						proxychecker.CheckProxies(ctx)
						cancel()
					}
				}
			}()

			// Wait for the first proxy check to complete
			audit.GlobalAuditor.Logger.Info("Waiting for initial image proxy check to complete...")
			wgFirstCheck.Wait()

			d := time.Since(startTime)
			truncated := d.Truncate(time.Millisecond)

			audit.GlobalAuditor.Logger.Infof("Initial image proxy check completed in %v.", truncated)
		}()
	} else {
		audit.GlobalAuditor.Logger.Infof("ProxyCheckEnabled set to %t, skipping image proxy checker initialization.", config.GlobalConfig.ProxyChecker.Enabled)
	}

	// Initialize cache
	requests.Init()
	audit.GlobalAuditor.Logger.Info("API response cache initialized.")

	audit.GlobalAuditor.Logger.Info("Starting server...")

	router := router.DefineRoutes()
	// the first middleware is the most outer / first executed one
	router.Use(middleware.WithRequestContext)  // needed for everything else
	router.Use(middleware.SetLocaleFromCookie) // needed for i18n.*()
	router.Use(middleware.LogRequest())        // all pages need this
	router.Use(middleware.SetResponseHeaders)  // all pages need this
	router.Use(middleware.HandleError)         // if the inner handler fails, this shows the error page instead

	// Limiter setup
	if config.GlobalConfig.Limiter.Enabled {
		limiterCfg := &limiter.Config{
			PassIPs:     config.GlobalConfig.Limiter.PassIPs,
			BlockIPs:    config.GlobalConfig.Limiter.BlockIPs,
			LinkToken:   config.GlobalConfig.Limiter.LinkToken,
			FilterLocal: config.GlobalConfig.Limiter.FilterLocal,
			IPv4Prefix:  config.GlobalConfig.Limiter.IPv4Prefix,
			IPv6Prefix:  config.GlobalConfig.Limiter.IPv6Prefix,
		}

		// Set the limiter config for limiter package
		limiter.SetLimiterConfig(limiterCfg)

		// Initialize the limiter middleware
		limiter.Init()

		// Add the limiter middleware to the router
		router.Use(limiter.Evaluate)
	}

	// router.Use(middleware.InitializeRateLimiter())

	// watch and compile sass when in development mode
	// if config.GlobalConfig.InDevelopment {
	// 	go run_sass(auditor)
	// }

	// Create http.Server instance
	server := &http.Server{
		Handler: router,
	}

	// Listen
	listener := chooseListener()

	// Start main server
	go func() {
		if err := server.Serve(listener); err != nil && err != http.ErrServerClosed {
			audit.GlobalAuditor.Logger.Fatalf("Starting http.Server failed: %v", err)
		}
	}()

	// Set up graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	audit.GlobalAuditor.Logger.Info("Server is shutting down...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		audit.GlobalAuditor.Logger.Fatalf("Server forced to shutdown: %v", err)
	}

	audit.GlobalAuditor.Logger.Info("Server exiting")
}

func run_sass(auditor *audit.Auditor) {
	cmd := exec.Command("sass", "--watch", "assets/css")
	cmd.Stdout = os.Stderr // Sass quirk
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true, Pdeathsig: syscall.SIGHUP}
	runtime.LockOSThread() // Go quirk https://github.com/golang/go/issues/27505
	err := cmd.Run()
	if err != nil {
		auditor.Logger.Errorf("When running sass: %v", err)
	}
}

func chooseListener() net.Listener {
	var listener net.Listener

	// Check if we should use a Unix domain socket
	if config.GlobalConfig.Basic.UnixSocket != "" {
		unixAddr := config.GlobalConfig.Basic.UnixSocket
		unixListener, err := net.Listen("unix", unixAddr)
		if err != nil {
			// Panic with the error description if unable to listen on Unix socket
			panic(fmt.Errorf("failed to listen on Unix socket %q: %w", unixAddr, err))
		}

		// Assign the listener and log where we are listening
		listener = unixListener
		audit.GlobalAuditor.Logger.Infof("Listening on Unix domain socket: %v", unixAddr)

	} else {
		// Otherwise, fall back to TCP listener
		addr := net.JoinHostPort(config.GlobalConfig.Basic.Host, config.GlobalConfig.Basic.Port)
		tcpListener, err := net.Listen("tcp", addr)
		if err != nil {
			// Panic with the error description if unable to listen on TCP
			audit.GlobalAuditor.Logger.Panicf("Failed to start TCP listener on %v: %v", addr, err)
		}

		// Assign the TCP listener
		listener = tcpListener
		addr = tcpListener.Addr().String()

		// Extract the host and port for logging
		_, port, err := net.SplitHostPort(addr)
		if err != nil {
			// Panic in case of invalid split into host and port
			panic(fmt.Errorf("error parsing listener address %q: %w", addr, err))
		}

		// Log the address and convenient URL for local development
		audit.GlobalAuditor.Logger.Infof("Listening on %v. Accessible at: http://pixivfe.localhost:%v/", addr, port)
	}

	return listener
}
