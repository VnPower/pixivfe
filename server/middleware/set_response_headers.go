// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package middleware

import (
	"net/http"
	"strings"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
)

// SetResponseHeaders adds default headers to HTTP responses.
func SetResponseHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Extract origins from content proxy URLs
		imageOrigin := session.GetOrigin(session.GetImageProxy(r))
		staticOrigin := session.GetOrigin(session.GetStaticProxy(r))
		ugoiraOrigin := session.GetOrigin(session.GetUgoiraProxy(r))

		// Prepare CSP origins
		// TrimSpace() allows us simple string concatenation for imgOrigins
		imgOrigins := strings.TrimSpace(imageOrigin + " " + staticOrigin)

		// mediaOrigins will only ever be ugoiraOrigin or empty, so just assign directly
		mediaOrigins := ugoiraOrigin

		setSecurityHeaders(w.Header(), imgOrigins, mediaOrigins, r.URL.Path)
		next.ServeHTTP(w, r)
	})
}

// setSecurityHeaders sets security headers.
//
// We don't set an HSTS header to avoid conflicts with reverse proxies.
// Generally better for reverse proxies to manage HSTS instead.
func setSecurityHeaders(headers http.Header, imgOrigins, mediaOrigins, path string) {
	// Standard security headers
	headers.Set("Referrer-Policy", "no-referrer")
	headers.Set("X-Frame-Options", "DENY")
	headers.Set("X-Content-Type-Options", "nosniff")
	// Setting CORP headers by default breaks compatibility with most external
	// image proxy servers, which don't set the required CORP cross-origin header
	// headers.Set("Cross-Origin-Embedder-Policy", "require-corp")
	// headers.Set("Cross-Origin-Opener-Policy", "same-origin")
	// headers.Set("Cross-Origin-Resource-Policy", "same-site")

	// Instance information
	headers.Set("Pixivfe-Version", config.GlobalConfig.Instance.Version)
	headers.Set("Pixivfe-Revision", config.GlobalConfig.Instance.Revision)
	headers.Set("X-Powered-By", "hatsune miku")

	setPermissionsPolicy(headers)
	setContentSecurityPolicy(headers, imgOrigins, mediaOrigins, path)
}

func setPermissionsPolicy(headers http.Header) {
	features := []string{
		"accelerometer=()",
		"ambient-light-sensor=()",
		"battery=()",
		"camera=()",
		"display-capture=()",
		"document-domain=()",
		"encrypted-media=()",
		"execution-while-not-rendered=()",
		"execution-while-out-of-viewport=()",
		"geolocation=()",
		"gyroscope=()",
		"magnetometer=()",
		"microphone=()",
		"midi=()",
		"navigation-override=()",
		"payment=()",
		"publickey-credentials-get=()",
		"screen-wake-lock=()",
		"sync-xhr=()",
		"usb=()",
		"web-share=()",
		"xr-spatial-tracking=()",
	}
	headers.Set("Permissions-Policy", strings.Join(features, ", "))
}

func setContentSecurityPolicy(headers http.Header, imgOrigins, mediaOrigins, path string) {
	if strings.HasPrefix(path, "/diagnostics") {
		return
	}

	// Build CSP directives that can have a dynamic
	// number of origins
	imgDirective := "img-src 'self' data:"
	if imgOrigins != "" {
		imgDirective += " " + imgOrigins
	}

	mediaDirective := "media-src 'self'"
	if mediaOrigins != "" {
		mediaDirective += " " + mediaOrigins
	}

	directives := []string{
		"base-uri 'self'",
		"default-src 'self'",
		"script-src 'self' 'unsafe-eval'", // Alpine.js requires unsafe-eval
		"script-src-elem 'self'",
		"style-src 'self' 'unsafe-inline'",
		imgDirective,
		mediaDirective,
		"font-src 'self'",
		"connect-src 'self'",
		"form-action 'self'",
		"frame-ancestors 'none'",
	}

	csp := strings.Join(directives, "; ") + ";"
	headers.Set("Content-Security-Policy", csp)
}
