// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package middleware

import (
	"net/http"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
	"codeberg.org/vnpower/pixivfe/v2/server/utils"
)

// ResponseWriterInterceptStatus wraps http.ResponseWriter to intercept the status code.
type ResponseWriterInterceptStatus struct {
	statusCode int
	http.ResponseWriter
}

// WriteHeader intercepts the status code before writing it to the response.
func (w *ResponseWriterInterceptStatus) WriteHeader(code int) {
	w.statusCode = code
	w.ResponseWriter.WriteHeader(code)
}

// LogRequest is a middleware that logs incoming HTTP requests and their corresponding responses.
func LogRequest() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if audit.ShouldSkipServerLogging(r.URL.Path) {
				// If skipping, simply call the next handler without logging
				next.ServeHTTP(w, r)

				return
			}

			// Wrap the ResponseWriter to capture the status code
			wrappedWriter := &ResponseWriterInterceptStatus{
				ResponseWriter: w,
			}

			start := time.Now()

			next.ServeHTTP(wrappedWriter, r)

			ctx := requestcontext.FromRequest(r)

			span := audit.Span{
				Component:  audit.ComponentServer,
				Duration:   time.Since(start),
				RequestID:  ctx.RequestID,
				Method:     r.Method,
				URL:        utils.Origin(r) + r.URL.String(),
				StatusCode: wrappedWriter.statusCode,
				Error:      ctx.RequestError,
			}
			audit.GlobalAuditor.LogAndRecord(span)
		})
	}
}
