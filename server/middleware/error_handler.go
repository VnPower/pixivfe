// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package middleware

import (
	"log"
	"maps"
	"net/http"
	"net/http/httptest"

	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
	"codeberg.org/vnpower/pixivfe/v2/server/routes"
)

// CatchError is a middleware that wraps HTTP handlers that return an error.
//
// It buffers the response using a httptest.ResponseRecorder. If the handler returns
// an error, the buffered response is discarded and the error is stored into the request
// context. Otherwise, the buffered response is copied to the real ResponseWriter.
//
// This pattern ensures that nothing is written to the client until we know the handler
// succeeded. It also avoids the complexity of manually backing up and restoring headers.
func CatchError(handler func(w http.ResponseWriter, r *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Create a recorder to capture the handler’s output.
		recorder := httptest.NewRecorder()

		// Execute the handler, capturing any error.
		if err := handler(recorder, r); err != nil {
			// On error, store it in the request context.
			requestcontext.FromRequest(r).RequestError = err
			// (Do not flush the buffered response to the client.)
			return
		}

		// If no error occurred, copy the buffered response headers, code, and body.
		maps.Copy(w.Header(), recorder.Header())
		w.WriteHeader(recorder.Code)

		if _, err := recorder.Body.WriteTo(w); err != nil {
			log.Printf("Error writing response: %v", err)
		}
	}
}

// HandleError is a middleware that wraps an http.Handler.
//
// After the wrapped handler executes,
// it checks for any error stored in the request context. If an error is found, it renders an error page,
// overriding any buffered response written previously.
//
// The separation between CatchError and HandleError allows for clean buffering versus final error rendering.
func HandleError(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Execute the wrapped handler.
		next.ServeHTTP(w, r)

		// Check if any error was caught during request processing.
		if err := requestcontext.FromRequest(r).RequestError; err != nil {
			routes.ErrorPage(w, r, err, http.StatusInternalServerError)
		}
	})
}
