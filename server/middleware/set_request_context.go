// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package middleware

import (
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
)

// WithRequestContext is a middleware that attaches a RequestContext to each HTTP request.
func WithRequestContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Create a new context with RequestContext attached
		ctxWithRequest := requestcontext.WithRequestContext(r.Context())

		// Create new request with the enhanced context
		reqWithContext := r.WithContext(ctxWithRequest)

		// Pass to the next handler
		next.ServeHTTP(w, reqWithContext)
	})
}
