// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

/*
Helpers used for testing
*/
package limiter

import (
	"testing"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/config"
)

// mockTimeProvider implements the timeProvider interface
// and maintains a controllable current time for testing.
type mockTimeProvider struct {
	currentTime time.Time
}

// newMockTimeProvider creates a new mockTimeProvider initialized with the given time.
//
// This allows tests to start from a specific point in time.
func newMockTimeProvider(initialTime time.Time) *mockTimeProvider {
	return &mockTimeProvider{
		currentTime: initialTime,
	}
}

// Now returns the current mock time
func (m *mockTimeProvider) Now() time.Time {
	return m.currentTime
}

// Sleep advances the mock current time by the specified duration
func (m *mockTimeProvider) Sleep(d time.Duration) {
	m.currentTime = m.currentTime.Add(d)
}

// setupLimiterTest prepares a test environment with a mock Redis server and time provider,
// as well as LimiterConfig configured with default settings.
//
// It returns the miniredis instance, Redis client, and mock time provider.
//
// The original time function is restored when the test completes.
func setupLimiterTest(t *testing.T) *mockTimeProvider {
	audit.NewTestingLogger(t)

	mockTime := newMockTimeProvider(time.Now())

	// Hook the mock time provider to the token bucket's timeNow
	origTimeNow := timeNow // Save original for restoring after tests
	timeNow = func() time.Time {
		return mockTime.Now()
	}

	// Register cleanup to restore the original timeNow function
	t.Cleanup(func() {
		timeNow = origTimeNow
	})

	// Set defaults
	config := &Config{
		LinkToken:   config.DefaultLimiterLinkToken,
		FilterLocal: config.DefaultLimiterFilterLocal,
		IPv4Prefix:  config.DefaultLimiterIPv4Prefix,
		IPv6Prefix:  config.DefaultLimiterIPv6Prefix,
	}

	SetLimiterConfig(config)

	return mockTime
}
