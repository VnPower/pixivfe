// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package limiter

import (
	"encoding/base64"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/mux"
)

// resetTokenStorage is a helper to reset the global token storage between tests.
func resetTokenStorage() {
	tokenStorage = &linkTokenStorage{
		tokens: make(map[string]tokenEntry),
	}
}

func TestLinkTokenHandler(t *testing.T) {
	setupLimiterTest(t)

	resetTokenStorage()

	t.Run("Valid token", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/token.css", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")

		// Calculate fingerprint the same way handler would
		fingerprint := generateClientFingerprint(r)

		// Create token with the calculated fingerprint
		css := []byte("/* test CSS */")
		token := tokenStorage.createToken(fingerprint, css)

		r = httptest.NewRequest(http.MethodGet, "/"+token+".css", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")
		r = mux.SetURLVars(r, map[string]string{"token": token})

		w := httptest.NewRecorder()
		if err := LinkTokenHandler(w, r); err != nil {
			t.Errorf("Expected no error, got: %v", err)
		}

		if w.Code != http.StatusOK {
			t.Errorf("Got status %d, want %d", w.Code, http.StatusOK)
		}

		// Check that we got CSS and an empty body is not returned
		if ct := w.Header().Get("Content-Type"); ct != "text/css" {
			t.Errorf("Expected text/css content type, got %q", ct)
		}
		if len(w.Body.Bytes()) == 0 {
			t.Error("Expected CSS content, got empty response")
		}

		// After consumption, token should no longer exist
		_, err := tokenStorage.getTokenByValue(token)
		if err == nil {
			t.Error("Token should have been consumed and removed, but still exists")
		}
	})

	t.Run("Invalid token", func(t *testing.T) {
		resetTokenStorage()
		token := "DOES_NOT_EXIST"

		r := httptest.NewRequest(http.MethodGet, "/"+token+".css", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")
		r = mux.SetURLVars(r, map[string]string{"token": token})

		w := httptest.NewRecorder()
		err := LinkTokenHandler(w, r)

		if w.Code != http.StatusNotFound {
			t.Errorf("Expected 404 for invalid token, got %d", w.Code)
		}
		if err == nil {
			t.Error("Expected an error for invalid token, got none")
		}
	})

	t.Run("Fingerprint mismatch", func(t *testing.T) {
		resetTokenStorage()
		// Create a token for one fingerprint
		fingerprint := "mismatch-fprint"
		token := tokenStorage.createToken(fingerprint, []byte("/* mismatch test */"))

		// But pass different IP/User-Agent so we generate a different fingerprint
		r := httptest.NewRequest(http.MethodGet, "/"+token+".css", nil)
		r.Header.Set("X-Real-IP", "2.2.2.2") // different IP => different fingerprint
		r.Header.Set("User-Agent", "test-agent")
		r = mux.SetURLVars(r, map[string]string{"token": token})

		w := httptest.NewRecorder()
		err := LinkTokenHandler(w, r)

		if w.Code != http.StatusNotFound {
			t.Errorf("Expected 404 for fingerprint mismatch, got %d", w.Code)
		}
		if err == nil {
			t.Error("Expected an error for fingerprint mismatch, got none")
		}
	})

	t.Run("Expired token", func(t *testing.T) {
		mockTime := setupLimiterTest(t)

		resetTokenStorage()

		fingerprint := "expire-fprint"
		token := tokenStorage.createToken(fingerprint, []byte("/* expiry test */"))

		// Wait until the token has expired
		mockTime.Sleep(61 * time.Second)

		// Attempt to consume
		r := httptest.NewRequest(http.MethodGet, "/"+token+".css", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")
		r = mux.SetURLVars(r, map[string]string{"token": token})

		w := httptest.NewRecorder()
		err := LinkTokenHandler(w, r)
		if w.Code != http.StatusNotFound {
			t.Errorf("Expected 404 for expired token, got %d", w.Code)
		}
		if err == nil {
			t.Error("Expected error for expired token, got none")
		}
	})
}

func TestGetOrCreateLinkToken(t *testing.T) {
	resetTokenStorage()

	t.Run("Generate new token and integrity", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")

		token, integrity, err := GetOrCreateLinkToken(r)
		if err != nil {
			t.Fatalf("Error generating token: %v", err)
		}
		if token == "" {
			t.Error("Expected non-empty token")
		}
		if !strings.HasPrefix(integrity, "sha256-") {
			t.Errorf("Integrity should start with sha256-, got %q", integrity)
		}

		// Ensure token is in storage
		entry, err := tokenStorage.getTokenByValue(token)
		if err != nil {
			t.Errorf("Expected token to be stored, got error: %v", err)
		}
		if entry == nil || entry.token != token {
			t.Error("Token entry not found in storage or mismatch")
		}
	})

	t.Run("Always generate fresh tokens", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/", nil)
		r.Header.Set("X-Real-IP", "1.1.1.1")
		r.Header.Set("User-Agent", "test-agent")

		token1, integrity1, err := GetOrCreateLinkToken(r)
		if err != nil {
			t.Fatalf("Error generating first token: %v", err)
		}
		token2, integrity2, err := GetOrCreateLinkToken(r)
		if err != nil {
			t.Fatalf("Error generating second token: %v", err)
		}
		if token1 == token2 {
			t.Error("Expected different tokens for each call, got the same value")
		}
		if integrity1 == integrity2 {
			t.Error("Expected different integrity hashes for each call, got the same value")
		}
	})
}

func TestUtilityFunctions(t *testing.T) {
	t.Run("calculateIntegrity returns expected format", func(t *testing.T) {
		content := []byte("/* test content */")
		intVal := calculateIntegrity(content)
		if !strings.HasPrefix(intVal, "sha256-") {
			t.Fatalf("Expected sha256- prefix, got %q", intVal)
		}

		// quick check that base64 decode works
		hashPart := strings.TrimPrefix(intVal, "sha256-")
		_, err := base64.StdEncoding.DecodeString(hashPart)
		if err != nil {
			t.Errorf("Failed to base64-decode integrity: %v", err)
		}
	})
}
