// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package limiter

import (
	"errors"
	"net"
	"net/http"
	"strings"

	"codeberg.org/vnpower/pixivfe/v2/audit"
)

var (
	ErrMissingClientIP = errors.New("missing client IP")
	ErrInvalidIPFormat = errors.New("invalid IP format")
	ErrNilNetwork      = errors.New("could not determine network")
)

// Client represents an HTTP request with associated network and rate limiting information.
//
// Instances are ephemeral and exist only for the duration of a single HTTP request lifecycle.
type Client struct {
	ip           net.IP
	network      net.IPNet
	isSuspicious bool
	limiter      *limiterWrapper
}

// newClient constructs a Client from an HTTP request, resolving IP and
// network, but does not run any checks yet.
func newClient(r *http.Request) (*Client, error) {
	realIP := getClientIP(r)
	if realIP == "" {
		return nil, ErrMissingClientIP
	}

	parsedIP := net.ParseIP(realIP)
	if parsedIP == nil {
		return nil, ErrInvalidIPFormat
	}

	network := getNetwork(parsedIP, limiterCfg.IPv4Prefix, limiterCfg.IPv6Prefix)
	if network == nil {
		return nil, ErrNilNetwork
	}

	return &Client{
		ip:      parsedIP,
		network: *network,
	}, nil
}

// checkIPLists checks if the client's IP is on the pass or block list.
//
// Returns (allowed, blocked) as a tuple - at most one can be true.
func (c *Client) checkIPLists() (bool, bool) {
	if c.isPassListed() {
		return true, false
	}

	if c.isBlockListed() {
		return false, true
	}

	return false, false
}

// validateLinkToken clears a client's suspicious status if it has
// successfully pinged its link token.
//
// Returns a bool with the result (true if not suspicious).
func (c *Client) validateLinkToken(r *http.Request) bool {
	if cookie, err := r.Cookie(pingCookieName); err == nil {
		if verifyPingCookie(cookie, r) {
			audit.GlobalAuditor.Logger.Debugln("Valid ping cookie found",
				"ip", c.ip.String())
			c.clearSuspiciousStatus()
			c.limiter = getOrCreateLimiter(c.network.String(), c.isSuspicious)
			updateNetworkHistory(c.limiter, c.network.String(), c.isSuspicious)

			return true
		}

		audit.GlobalAuditor.Logger.Warnln("Invalid ping cookie",
			"ip", c.ip.String())
	}

	// No valid cookie or invalid cookie, client is suspicious
	c.markSuspicious()
	c.limiter = getOrCreateLimiter(c.network.String(), c.isSuspicious)
	updateNetworkHistory(c.limiter, c.network.String(), c.isSuspicious)

	return false
}

// isFullyExcludedPath returns true if the request path matches any
// of the fullyExcludedPaths (skip all checks).
func (c *Client) isFullyExcludedPath(r *http.Request) bool {
	path := r.URL.Path
	for _, p := range excludedPaths {
		if strings.HasPrefix(path, p) {
			return true
		}
	}

	return false
}

// isPassListed returns true if c.IP is in the configured pass list.
func (c *Client) isPassListed() bool {
	return ipMatchesList(c.ip, limiterCfg.PassIPs)
}

// isBlockListed returns true if c.IP is in the configured block list.
func (c *Client) isBlockListed() bool {
	return ipMatchesList(c.ip, limiterCfg.BlockIPs)
}

// isLocalLink returns true if c.IP is a link-local address.
//
// Supports both IPv4 and IPv6.
func (c *Client) isLocalLink() bool {
	if c.ip.To4() != nil {
		// IPv4
		return c.ip.IsLinkLocalUnicast() // covers 169.254.0.0/16
	}
	// IPv6
	return c.ip.IsLinkLocalUnicast() // covers fe80::/10
}

// blockedByHeaders checks for suspicious request headers.
//
// Returns a non-empty string with the block reason if blocked.
func (c *Client) blockedByHeaders(r *http.Request) string {
	return blockedByHeaders(r)
}

// clearSuspiciousStatus sets the IsSuspicious field for this client
// to false, effectively giving it a clean slate.
func (c *Client) clearSuspiciousStatus() {
	c.isSuspicious = false
}

// markSuspicious sets the IsSuspicious field of the client to true.
func (c *Client) markSuspicious() {
	c.isSuspicious = true
}
