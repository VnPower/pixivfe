// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package limiter

import (
	"net"
	"net/http"
	"strings"

	"codeberg.org/vnpower/pixivfe/v2/audit"
)

// IPv4 and IPv6 address bit lengths.
const (
	ipv4BitLength = 32  // Standard length of an IPv4 address in bits
	ipv6BitLength = 128 // Standard length of an IPv6 address in bits
)

// getClientIP extracts the client's IP address from an HTTP request.
//
// It checks the following sources, in order of priority:
// 1. X-Real-IP header.
// 2. Last IP in X-Forwarded-For header.
// 3. Request's RemoteAddr.
func getClientIP(r *http.Request) string {
	audit.GlobalAuditor.Logger.Debugln("Resolving client IP",
		"remote_addr", r.RemoteAddr,
		"x_forwarded_for", r.Header.Get("X-Forwarded-For"),
		"x_real_ip", r.Header.Get("X-Real-IP"))

	// Extract the base IP from RemoteAddr, ignoring the port if one is present.
	// For example, "192.168.1.10:8080" produces "192.168.1.10".
	remoteIP := r.RemoteAddr
	if ip, _, err := net.SplitHostPort(remoteIP); err == nil {
		remoteIP = ip
	}

	// Retrieve the X-Forwarded-For (XFF) header and parse out the last IP if multiple are present.
	// The "last IP" in XFF is typically the one added by the most recent proxy, so we use that.
	xff := strings.TrimSpace(r.Header.Get("X-Forwarded-For"))
	if xff == "" {
		audit.GlobalAuditor.Logger.Debugln("Missing X-Forwarded-For header",
			"remote_ip", remoteIP)
	}

	var xffIP string

	if xff != "" {
		parts := strings.Split(xff, ",")
		xffIP = strings.TrimSpace(parts[len(parts)-1])
	}

	// If an X-Real-IP header is set, check it for consistency against XFF and RemoteAddr.
	// Return X-Real-IP when present, but log warnings if it disagrees with other values.
	realIP := strings.TrimSpace(r.Header.Get("X-Real-IP"))
	if realIP != "" {
		// Warn if X-Real-IP is not found in the XFF header (when XFF exists).
		if xff != "" && !strings.Contains(xff, realIP) {
			audit.GlobalAuditor.Logger.Warnln("X-Real-IP differs from X-Forwarded-For",
				"real_ip", realIP,
				"xff", xff)
		}
		// Warn if X-Real-IP differs from the IP in RemoteAddr.
		if remoteIP != "" && realIP != remoteIP {
			audit.GlobalAuditor.Logger.Warnln("X-Real-IP differs from RemoteAddr",
				"real_ip", realIP,
				"remote_addr", remoteIP)
		}
		return realIP
	}

	// If X-Real-IP is not set, prefer the last XFF IP if available.
	if xffIP != "" {
		return xffIP
	}

	// If all headers are missing or empty, use the parsed RemoteAddr IP as a fallback.
	if remoteIP != "" {
		audit.GlobalAuditor.Logger.Debugln("Using RemoteAddr as fallback", "ip", remoteIP)
		return remoteIP
	}

	// As a final fallback, return an empty string if no IP could be determined.
	audit.GlobalAuditor.Logger.Errorln("Could not determine client IP")
	return ""
}

// ipMatchesList checks if an IP is within any of the provided CIDRs or matches them exactly.
func ipMatchesList(rawIP net.IP, cidrs []string) bool {
	audit.GlobalAuditor.Logger.Debugln("Checking IP against list",
		"ip", rawIP,
		"list_size", len(cidrs))

	// Store IP string representation to avoid repeated conversions
	ipStr := rawIP.String()

	for _, cidr := range cidrs {
		// Check for an exact match first
		if ipStr == cidr {
			audit.GlobalAuditor.Logger.Debugln("IP matched exactly",
				"ip", ipStr,
				"match", cidr)
			return true
		}

		// Try parsing as CIDR and check if IP is within subnet
		_, subnet, err := net.ParseCIDR(cidr)
		if err == nil && subnet.Contains(rawIP) {
			audit.GlobalAuditor.Logger.Debugln("IP matched CIDR",
				"ip", ipStr,
				"cidr", cidr)
			return true
		}
	}

	// No match found in any CIDR or exact comparison
	return false
}

func getNetwork(rawIP net.IP, ipv4Prefix, ipv6Prefix int) *net.IPNet {
	// Create mask based on IP version and configured prefix
	var mask net.IPMask
	if rawIP.To4() != nil {
		// IPv4
		mask = net.CIDRMask(ipv4Prefix, ipv4BitLength)
	} else {
		// IPv6
		mask = net.CIDRMask(ipv6Prefix, ipv6BitLength)
	}

	// Create network with the IP and determined mask
	return &net.IPNet{
		IP:   rawIP.Mask(mask),
		Mask: mask,
	}
}
