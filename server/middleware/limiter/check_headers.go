// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package limiter

import (
	"net/http"
	"path/filepath"
	"sort"
	"strings"

	"codeberg.org/vnpower/pixivfe/v2/audit"
)

// blockedByHeaders checks a handful of typical "browser" headers, returning
// a non-empty string with the block reason if something is suspicious enough to block.
func blockedByHeaders(r *http.Request) string {
	// User-Agent check: if missing or in a known-bot list, block.
	if reason := checkUserAgent(r); reason != "" {
		return reason
	}

	// Accept check for various file types
	if reason := checkAcceptHeader(r.URL.Path, r.Header.Get("Accept")); reason != "" {
		audit.GlobalAuditor.Logger.Warnln("Blocked by Accept header - " + reason)

		return "Blocked by Accept header - " + reason
	}

	// Accept-Encoding check: must contain gzip or deflate
	if reason := checkAcceptEncoding(r); reason != "" {
		return reason
	}

	// Accept-Language check: cannot be empty
	if reason := checkAcceptLanguage(r); reason != "" {
		return reason
	}

	if reason := checkSecFetch(r); reason != "" {
		return reason
	}

	// Connection check: block if "close"
	//
	// This check is actually quite useless as when the application is behind a reverse proxy,
	// all incoming connections will likely be over HTTP/1.1 with Connection: keep-alive anyway
	//
	// SearXNG disables this check for a different reason related to uSWGI behavior,
	// see https://github.com/searxng/searxng/issues/2892
	//
	// conn := r.Header.Get("Connection")
	// if strings.EqualFold(strings.TrimSpace(conn), "close") {
	// 	return "Blocked by Connection=close"
	// }

	audit.GlobalAuditor.Logger.Debugln("All header checks passed")

	return ""
}

func checkUserAgent(r *http.Request) string {
	userAgent := r.Header.Get("User-Agent")
	audit.GlobalAuditor.Logger.Debugln("Checking User-Agent header",
		"user_agent", userAgent)

	if userAgent == "" || isBotUserAgent(userAgent) {
		audit.GlobalAuditor.Logger.Warnln("Blocked by User-Agent check",
			"user_agent", userAgent)

		return "Blocked by user-agent check"
	}

	return ""
}

// isBotUserAgent checks if the user-agent matches known bots or suspicious patterns.
func isBotUserAgent(userAgent string) bool {
	botSubstrings := []string{
		"abonti",
		"ahrefsbot",
		"archive.org_bot",
		"baiduspider",
		"bingbot",
		"blexbot",
		"bitlybot",
		"curl",
		"exabot",
		"farside/0.1.0",
		"feedfetcher",
		"go-http-client",
		"googlebot",
		"googleimageproxy",
		"headlesschrome",
		"httpclient",
		"jakarta",
		"james bot",
		"java",
		"javafx",
		"jersey",
		"libwww-perl",
		"linkdexbot",
		"mj12bot",
		"msnbot",
		"netvibes",
		"okhttp",
		"petalbot",
		"pixray",
		"python",
		"python-requests",
		"ruby",
		"scrapy",
		"semrushbot",
		"seznambot",
		"sogou",
		"spinn3r",
		"splash",
		"synhttpclient",
		"universalfeedparser",
		"unknown",
		"wget",
		"yahoo! slurp",
		"yacybot",
		"yandexbot",
		"yandexmobilebot",
		"zmeu",
	}

	s := strings.ToLower(userAgent)
	for _, sub := range botSubstrings {
		if strings.Contains(s, sub) {
			audit.GlobalAuditor.Logger.Warnln("Detected bot User-Agent",
				"pattern", sub,
				"user_agent", userAgent)

			return true
		}
	}

	return false
}

// checkAcceptHeader validates the Accept header for a given file path.
//
// Returns an error message if the Accept header doesn't include the required MIME type.
func checkAcceptHeader(path, accept string) string {
	ext := strings.ToLower(filepath.Ext(path))

	audit.GlobalAuditor.Logger.Debugln("Checking Accept header",
		"accept", accept,
		"path", path,
		"extension", ext)

	// Accept */* matches all MIME types
	if strings.Contains(accept, "*/*") {
		return ""
	}

	var (
		required     []string
		errorMessage string
	)

	switch ext {
	case ".js":
		// JavaScript files: require either application/javascript or text/javascript.
		required = []string{"application/javascript", "text/javascript"}
		errorMessage = "JavaScript file requires JavaScript Accept type"
	case ".css":
		required = []string{"text/css"}
		errorMessage = "CSS file requires text/css Accept type"
	case ".png", ".jpeg", ".jpg", ".gif", ".svg":
		required = []string{"image/"}
		errorMessage = "Image file requires image/* Accept type"
	case ".json":
		required = []string{"application/json"}
		errorMessage = "JSON file requires application/json Accept type"
	case ".txt", ".map", ".scss":
		required = []string{"text/plain"}
		errorMessage = "Text file requires text/plain Accept type"
	default: // HTML files and root paths
		required = []string{"text/html"}
		errorMessage = "HTML file requires text/html Accept type"
	}

	// Check if any of the required MIME types is included. For .js, if any match is found, that's sufficient.
	for _, req := range required {
		if strings.Contains(accept, req) {
			return ""
		}
	}

	return errorMessage
}

func checkAcceptEncoding(r *http.Request) string {
	enc := r.Header.Get("Accept-Encoding")
	audit.GlobalAuditor.Logger.Debugln("Checking Accept-Encoding header",
		"encoding", enc)

	encLower := strings.ToLower(enc)
	if !strings.Contains(encLower, "gzip") && !strings.Contains(encLower, "deflate") {
		audit.GlobalAuditor.Logger.Warnln("Blocked by Accept-Encoding check")

		return "Blocked by Accept-Encoding check"
	}

	return ""
}

func checkAcceptLanguage(r *http.Request) string {
	lang := r.Header.Get("Accept-Language")
	audit.GlobalAuditor.Logger.Debugln("Checking Accept-Language header",
		"lang", lang)

	if strings.TrimSpace(lang) == "" {
		audit.GlobalAuditor.Logger.Warnln("Blocked by Accept-Language check")

		return "Blocked by Accept-Language check"
	}

	return ""
}

// checkSecFetch checks whether the fetch metadata headers for
// a request are present and non-empty.
//
// Note that SearXNG doesn't actually check these headers
// for browser compatibility reasons, see https://github.com/searxng/searxng/pull/3965
func checkSecFetch(r *http.Request) string {
	headers := map[string]string{
		"Sec-Fetch-Mode": r.Header.Get("Sec-Fetch-Mode"),
		"Sec-Fetch-Site": r.Header.Get("Sec-Fetch-Site"),
		"Sec-Fetch-Dest": r.Header.Get("Sec-Fetch-Dest"),
	}

	audit.GlobalAuditor.Logger.Debug("Checking Sec-Fetch headers",
		"mode", headers["Sec-Fetch-Mode"],
		"site", headers["Sec-Fetch-Site"],
		"dest", headers["Sec-Fetch-Dest"])

	var missingHeaders []string

	for headerName, headerValue := range headers {
		if len(headerValue) == 0 {
			audit.GlobalAuditor.Logger.Warnln("Missing " + headerName + " header")
			missingHeaders = append(missingHeaders, headerName)
		}
	}

	switch len(missingHeaders) {
	case 0:
		return ""
	case 1:
		return "Missing " + missingHeaders[0] + " header"
	default:
		sort.Strings(missingHeaders)

		return "Missing Sec-Fetch headers: " + strings.Join(missingHeaders, ", ")
	}
}
