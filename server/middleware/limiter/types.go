// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package limiter

// limiterCfg holds the limiter configuration
var limiterCfg *Config

// SetLimiterConfig sets the limiter configuration for use in routes
func SetLimiterConfig(cfg *Config) {
	limiterCfg = cfg
}

// Config holds user-configurable options for Limiter.
type Config struct {
	PassIPs     []string // CIDRs or IPs that get a free pass
	BlockIPs    []string // CIDRs or IPs that are always blocked
	LinkToken   bool     // If true, enables "link token" logic
	FilterLocal bool     // Whether to filter link-local addresses
	IPv4Prefix  int
	IPv6Prefix  int
}
