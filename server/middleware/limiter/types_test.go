package limiter

import (
	"reflect"
	"testing"
)

func TestSetLimiterConfig(t *testing.T) {
	// Reset global config before test
	limiterCfg = nil

	// Test setting nil config
	t.Run("Set nil config", func(t *testing.T) {
		SetLimiterConfig(nil)
		if limiterCfg != nil {
			t.Error("Expected nil config, got non-nil")
		}
	})

	// Test setting valid config
	t.Run("Set valid config", func(t *testing.T) {
		testConfig := &Config{
			PassIPs:     []string{"127.0.0.1"},
			BlockIPs:    []string{"10.0.0.1"},
			LinkToken:   true,
			FilterLocal: false,
			IPv4Prefix:  24,
			IPv6Prefix:  64,
		}

		SetLimiterConfig(testConfig)

		if !reflect.DeepEqual(limiterCfg, testConfig) {
			t.Errorf("Config not set correctly. Got %+v, want %+v", limiterCfg, testConfig)
		}
	})

	// Test all fields are properly set
	t.Run("Check all fields", func(t *testing.T) {
		testConfig := &Config{
			PassIPs:     []string{"192.168.1.1"},
			BlockIPs:    []string{"172.16.0.1"},
			LinkToken:   true,
			FilterLocal: false,
			IPv4Prefix:  16,
			IPv6Prefix:  48,
		}

		SetLimiterConfig(testConfig)

		if !reflect.DeepEqual(limiterCfg.PassIPs, testConfig.PassIPs) {
			t.Error("PassIPs not set correctly")
		}
		if !reflect.DeepEqual(limiterCfg.BlockIPs, testConfig.BlockIPs) {
			t.Error("BlockIPs not set correctly")
		}
		if limiterCfg.LinkToken != testConfig.LinkToken {
			t.Error("LinkToken not set correctly")
		}
		if limiterCfg.FilterLocal != testConfig.FilterLocal {
			t.Error("FilterLocal not set correctly")
		}
		if limiterCfg.IPv4Prefix != testConfig.IPv4Prefix {
			t.Error("IPv4Prefix not set correctly")
		}
		if limiterCfg.IPv6Prefix != testConfig.IPv6Prefix {
			t.Error("IPv6Prefix not set correctly")
		}
	})
}
