// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"fmt"
	"html"
	"html/template"
	"math"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/i18n"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
)

// HTML is a type alias for template.HTML.
type HTML = template.HTML

// PageInfo represents information about a single page in pagination.
type PageInfo struct {
	Number int
	URL    string
}

// PaginationData contains all necessary information for rendering pagination controls.
type PaginationData struct {
	CurrentPage   int
	MaxPage       int
	Pages         []PageInfo
	HasPrevious   bool
	HasNext       bool
	PreviousURL   string
	NextURL       string
	FirstURL      string
	LastURL       string
	HasMaxPage    bool
	LastPage      int
	DropdownPages []PageInfo
}

// RelativeTimeData holds the numeric value and description for relative time.
type RelativeTimeData struct {
	Value       string
	Description string
	Time        string
}

// emojiList is a map of emoji shortcodes to their corresponding image IDs.
var emojiList = map[string]string{
	"normal":        "101",
	"surprise":      "102",
	"serious":       "103",
	"heaven":        "104",
	"happy":         "105",
	"excited":       "106",
	"sing":          "107",
	"cry":           "108",
	"normal2":       "201",
	"shame2":        "202",
	"love2":         "203",
	"interesting2":  "204",
	"blush2":        "205",
	"fire2":         "206",
	"angry2":        "207",
	"shine2":        "208",
	"panic2":        "209",
	"normal3":       "301",
	"satisfaction3": "302",
	"surprise3":     "303",
	"smile3":        "304",
	"shock3":        "305",
	"gaze3":         "306",
	"wink3":         "307",
	"happy3":        "308",
	"excited3":      "309",
	"love3":         "310",
	"normal4":       "401",
	"surprise4":     "402",
	"serious4":      "403",
	"love4":         "404",
	"shine4":        "405",
	"sweat4":        "406",
	"shame4":        "407",
	"sleep4":        "408",
	"heart":         "501",
	"teardrop":      "502",
	"star":          "503",
}

// parseEmojis replaces emoji shortcodes in a string with corresponding image tags.
func parseEmojis(s string) HTML {
	// Regular expression to match emoji shortcodes
	regex := regexp.MustCompile(`\(([^)]+)\)`)

	// Replace shortcodes with corresponding image tags
	parsedString := regex.ReplaceAllStringFunc(s, func(s string) string {
		s = s[1 : len(s)-1] // Get the string inside parentheses

		emojiID, found := emojiList[s] // Check if the string has a corresponding emoji ID
		if !found {
			return fmt.Sprintf("(%s)", s) // No replacement, return the original text
		}

		// FIXME: this ignores any user-set static image proxy, but you
		// can't pass *http.Request from within a Jet template
		//
		// code needs to be reworked so that this is called before RenderHTML
		proxy, err := session.GetProxyPrefix(config.GlobalConfig.ContentProxies.Static)
		if err != nil {
			return ""
		}

		return fmt.Sprintf(`<img src="%s/common/images/emoji/%s.png" alt="(%s)" class="emoji" />`, proxy, emojiID, s)
	})

	return HTML(parsedString)
}

// parsePixivRedirect extracts and unescapes URLs from pixiv's redirect links.
func parsePixivRedirect(s string) HTML {
	// Regular expression to match Pixiv's redirect URLs
	regex := regexp.MustCompile(`\/jump\.php\?(http[^"]+)`)

	// Extract the actual URL from the redirect link
	parsedString := regex.ReplaceAllStringFunc(s, func(s string) string {
		s = s[10:]

		return s
	})

	// Unescape the URL
	escaped, err := url.QueryUnescape(parsedString)
	if err != nil {
		return HTML(s)
	}

	return HTML(escaped)
}

// escapeString escapes a string for use in a URL query.
func escapeString(s string) string {
	escaped := url.QueryEscape(s)

	return escaped
}

// unescapeHTMLString unescapes all HTML entities to literal characters.
func unescapeHTMLString(s string) string {
	return html.UnescapeString(s)
}

// parseTime formats a time.Time value as a string in the format "2006-01-02 15:04".
func parseTime(date time.Time) string {
	return date.Format("2006-01-02 15:04")
}

// parseTimeCustomFormat formats a time.Time value as a string using a custom format.
func parseTimeCustomFormat(date time.Time, format string) string {
	return date.Format(format)
}

// naturalTime formats a time.Time value as a natural language string.
//
// TODO: tailor the format per locale.
func naturalTime(date time.Time) string {
	return date.Format("Monday, 2 January 2006, at 3:04 PM")
}

// relativeTime returns a RelativeTimeData struct with a relative description based on the given date.
//
// The "Yesterday" case is special and is triggered when the date matches exactly the previous
// calendar day (i.e., same year, month, and previous day), regardless of the exact number of
// hours that have elapsed.
//
// TODO: tailor the format per locale.
func relativeTime(date time.Time) RelativeTimeData {
	now := time.Now()
	duration := now.Sub(date)

	// local helper function to choose the correct singular/plural unit.
	pluralize := func(value int, singular, plural string) string {
		if value == 1 {
			return singular
		}

		return plural
	}

	// For future dates, simply show the day and full date/time formatting.
	if duration < 0 {
		return RelativeTimeData{
			Value:       date.Format("2"),
			Description: date.Format("January 2006 3:04 PM"),
		}
	}

	// Less than one minute ago.
	if duration < time.Minute {
		return RelativeTimeData{
			Value: "Just now",
		}
	}

	// Less than one hour: display minutes.
	if duration < time.Hour {
		minutes := int(duration.Minutes())

		return RelativeTimeData{
			Value:       fmt.Sprintf("%d %s", minutes, pluralize(minutes, "minute", "minutes")),
			Description: "ago",
		}
	}

	// Less than one day: display hours.
	if duration < 24*time.Hour {
		hours := int(duration.Hours())

		return RelativeTimeData{
			Value:       fmt.Sprintf("%d %s", hours, pluralize(hours, "hour", "hours")),
			Description: "ago",
		}
	}

	// Check if the date corresponds to 'yesterday'
	yesterday := now.AddDate(0, 0, -1)
	if date.Year() == yesterday.Year() && date.Month() == yesterday.Month() && date.Day() == yesterday.Day() {
		return RelativeTimeData{
			Value:       "Yesterday",
			Description: "at",
			Time:        date.Format("3:04 PM"),
		}
	}

	// Less than one week: display days.
	if duration < 7*24*time.Hour {
		days := int(duration.Hours() / 24)

		return RelativeTimeData{
			Value:       fmt.Sprintf("%d %s", days, pluralize(days, "day", "days")),
			Description: "ago",
		}
	}

	// Less than one month (using a 31-day threshold): display weeks.
	if duration < 31*24*time.Hour {
		weeks := int(duration.Hours() / (24 * 7))

		return RelativeTimeData{
			Value:       fmt.Sprintf("%d %s", weeks, pluralize(weeks, "week", "weeks")),
			Description: "ago",
		}
	}

	// Calculate total month difference (ignoring day differences for simplicity).
	yearDiff := now.Year() - date.Year()
	monthDiff := int(now.Month()) - int(date.Month())
	months := yearDiff*12 + monthDiff

	// Less than one year: display months.
	if months < 12 {
		return RelativeTimeData{
			Value:       fmt.Sprintf("%d %s", months, pluralize(months, "month", "months")),
			Description: "ago",
		}
	}

	// Otherwise, show years.
	years := months / 12

	return RelativeTimeData{
		Value:       fmt.Sprintf("%d %s", years, pluralize(years, "year", "years")),
		Description: "ago",
	}
}

// prettyNumber pretty prints an integer with commas as thousands separators.
//
// Negative numbers are handled by first setting aside the sign.
func prettyNumber(n int) string {
	// Determine sign
	sign := ""
	if n < 0 {
		sign = "-"
		n = -n // work with an absolute value
	}

	// Convert the integer to a string using strconv.Itoa.
	numStr := strconv.Itoa(n)

	// If there are no more than three digits, no comma is needed.
	if len(numStr) <= 3 {
		return sign + numStr
	}

	// Build digit groups from right to left.
	var groups []string
	for len(numStr) > 3 {
		// Extract the last three digits.
		groups = append([]string{numStr[len(numStr)-3:]}, groups...)
		// Remove the processed group.
		numStr = numStr[:len(numStr)-3]
	}
	// Prepend any remaining digits.
	if len(numStr) > 0 {
		groups = append([]string{numStr}, groups...)
	}

	// Join the groups with commas, and add a preceding negative sign if needed.
	return sign + strings.Join(groups, ",")
}

// ordinalNumeral returns an integer as its ordinal form in a string.
func ordinalNumeral(num int) string {
	// Handle special cases for 11, 12, 13
	lastTwo := num % 100
	if lastTwo == 11 || lastTwo == 12 || lastTwo == 13 {
		return strconv.Itoa(num) + "th"
	}

	// Handle cases based on last digit
	lastDigit := num % 10
	switch lastDigit {
	case 1:
		return strconv.Itoa(num) + "st"
	case 2:
		return strconv.Itoa(num) + "nd"
	case 3:
		return strconv.Itoa(num) + "rd"
	default:
		return strconv.Itoa(num) + "th"
	}
}

// createPaginator generates pagination data based on the current page and maximum number of pages.
// It returns a PaginationData struct containing all necessary information for rendering pagination controls.
//
// Parameters:
// - base: The base part of the pagination URL.
// - ending: The ending part of the pagination URL.
// - current_page: The current page being displayed.
// - max_page: Maximum number of pages (-1 if unknown).
// - page_margin: Number of pages to display before and after the current page.
// - dropdown_offset: Number of pages to include in dropdown before and after current page.
func createPaginator(base, ending string, current_page, max_page, page_margin, dropdown_offset int) (PaginationData, error) {
	// Validate input parameters
	if current_page < 1 {
		return PaginationData{}, fmt.Errorf("current_page must be greater than or equal to 1, got %d", current_page)
	}

	if page_margin < 0 {
		return PaginationData{}, fmt.Errorf("page_margin must be non-negative, got %d", page_margin)
	}

	if dropdown_offset < 0 {
		return PaginationData{}, fmt.Errorf("dropdown_offset must be non-negative, got %d", dropdown_offset)
	}

	// Validation for users that don't have any artworks
	// NOTE: the following breaks the current max_page implementation, commenting it out for now
	// if max_page < 1 {
	// 	max_page = 1
	// }

	// Validation for max_page in relation to current_page
	// if max_page < current_page {
	// 	return PaginationData{}, fmt.Errorf("max_page (%d) must be greater than or equal to current_page (%d) when specified", max_page, current_page)
	// }

	hasMaxPage := max_page != -1

	pageURL := func(page int) string {
		return fmt.Sprintf(`%s%d%s`, base, page, ending)
	}

	// Helper function to generate a range of pages
	generatePageRange := func(start, end int) []PageInfo {
		if start > end {
			return []PageInfo{}
		}

		pages := make([]PageInfo, 0, end-start+1)
		for i := start; i <= end; i++ {
			pages = append(pages, PageInfo{Number: i, URL: pageURL(i)})
		}

		return pages
	}

	// Calculate the range of pages to display
	start := max(1, current_page-page_margin)

	end := current_page + page_margin
	if hasMaxPage {
		end = min(max_page, end)
	}

	// Generate page information for the range
	pages := generatePageRange(start, end)

	var lastPage int
	if len(pages) > 0 {
		lastPage = pages[len(pages)-1].Number
	} else {
		lastPage = current_page
	}

	// Generate dropdown pages
	dropdownStart := max(1, current_page-dropdown_offset)

	dropdownEnd := current_page + dropdown_offset
	if hasMaxPage {
		dropdownEnd = min(max_page, dropdownEnd)
	}

	dropdownPages := generatePageRange(dropdownStart, dropdownEnd)

	// Calculate previous and next URLs
	var previousURL, nextURL string
	if current_page > 1 {
		previousURL = pageURL(current_page - 1)
	}

	if !hasMaxPage || current_page < max_page {
		nextURL = pageURL(current_page + 1)
	}

	// Create and return the PaginationData struct
	return PaginationData{
		CurrentPage:   current_page,
		MaxPage:       max_page,
		Pages:         pages,
		HasPrevious:   current_page > 1,
		HasNext:       !hasMaxPage || current_page < max_page,
		PreviousURL:   previousURL,
		NextURL:       nextURL,
		FirstURL:      pageURL(1),
		LastURL:       pageURL(max_page),
		HasMaxPage:    hasMaxPage,
		LastPage:      lastPage,
		DropdownPages: dropdownPages,
	}, nil
}

var genreMap = map[string]string{
	"1":  "Romance",
	"2":  "Isekai fantasy",
	"3":  "Contemporary fantasy",
	"4":  "Mystery",
	"5":  "Horror",
	"6":  "Sci-fi",
	"7":  "Literature",
	"8":  "Drama",
	"9":  "Historical pieces",
	"10": "BL (yaoi)",
	"11": "Yuri",
	"12": "For kids",
	"13": "Poetry",
	"14": "Essays/non-fiction",
	"15": "Screenplays/scripts",
	"16": "Reviews/opinion pieces",
	"17": "Other",
}

// getNovelGenre returns the genre name for a given genre ID.
func getNovelGenre(s string) string {
	if genre, ok := genreMap[s]; ok {
		return i18n.Tr(genre)
	}
	return i18n.Sprintf("(Unknown Genre: %s)", s)
}

// isFirstPathPart checks if the first part of the current path matches the given path.
func isFirstPathPart(currentPath, pathToCheck string) bool {
	// Trim any trailing slashes from both paths
	currentPath = strings.TrimRight(currentPath, "/")
	pathToCheck = strings.TrimRight(pathToCheck, "/")

	// Split the current path into parts
	parts := strings.SplitN(currentPath, "/", 3)

	// Check if we have at least two parts (empty string and the first path part)
	if len(parts) < 2 {
		return false
	}

	// Compare the first path part with the pathToCheck
	return "/"+parts[1] == pathToCheck
}

// isLastPathPart checks if the last part of the current path matches the given path.
func isLastPathPart(currentPath, pathToCheck string) bool {
	// Parse the currentPath to remove query parameters
	u, err := url.Parse(currentPath)
	if err == nil {
		currentPath = u.Path
	}

	// Trim any trailing slashes from both paths
	currentPath = strings.TrimRight(currentPath, "/")
	pathToCheck = strings.TrimRight(pathToCheck, "/")

	// Split the current path into parts
	parts := strings.Split(currentPath, "/")

	// Check if there is at least one part
	if len(parts) < 1 {
		return false
	}

	// Get the last part
	lastPart := parts[len(parts)-1]

	// Compare the last path part with the pathToCheck
	return "/"+lastPart == pathToCheck
}

// isFullPath checks if the entire current path matches the given path,
// excluding query parameters and fragments.
func isFullPath(currentPath, pathToCheck string) bool {
	// Parse the currentPath to remove query parameters and fragment
	u, err := url.Parse(currentPath)
	if err == nil {
		currentPath = u.Path
	}

	// Trim any trailing slashes from both paths
	currentPath = strings.TrimRight(currentPath, "/")
	pathToCheck = strings.TrimRight(pathToCheck, "/")

	// Compare the cleaned paths
	return currentPath == pathToCheck
}

// formatWorkIDs formats an integer slice of work IDs
// into a comma-separated string.
//
// Used to correctly format work IDs for the
// recent works API call in artworkFast.jet.html.
func formatWorkIDs(ids []int) string {
	if len(ids) == 0 {
		return ""
	}

	// Convert ints to strings
	strIDs := make([]string, len(ids))
	for i, id := range ids {
		strIDs[i] = strconv.Itoa(id)
	}

	// Join with commas
	return strings.Join(strIDs, ",")
}

var (
	furiganaPattern = regexp.MustCompile(`\[\[rb:\s*(.+?)\s*>\s*(.+?)\s*\]\]`)
	chapterPattern  = regexp.MustCompile(`\[chapter:\s*(.+?)\s*\]`)
	jumpURIPattern  = regexp.MustCompile(`\[\[jumpuri:\s*(.+?)\s*>\s*(.+?)\s*\]\]`)
	jumpPagePattern = regexp.MustCompile(`\[jump:\s*(\d+?)\s*\]`)
	newPagePattern  = regexp.MustCompile(`\s*\[newpage\]\s*`)
)

func ParseNovelContent(s string) HTML {
	// Replace furigana markup with HTML ruby tags
	furiganaTemplate := `<ruby>$1<rp>(</rp><rt>$2</rt><rp>)</rp></ruby>`
	s = furiganaPattern.ReplaceAllString(s, furiganaTemplate)

	// Replace chapter markup with HTML h2 tags
	chapterTemplate := `<h2>$1</h2>`
	s = chapterPattern.ReplaceAllString(s, chapterTemplate)

	// Replace jump URI markup with HTML anchor tags
	jumpURITemplate := `<a href="$2" target="_blank">$1</a>`
	s = jumpURIPattern.ReplaceAllString(s, jumpURITemplate)

	// Replace jump page markup with HTML anchor tags
	jumpPageTemplate := `<a href="#$1">To page $1</a>`
	s = jumpPagePattern.ReplaceAllString(s, jumpPageTemplate)

	// Handle newpage markup
	if strings.Contains(s, "[newpage]") {
		// Prepend <hr id="1"/> to the page if [newpage] is present
		s = `<hr id="1"/>` + s
		pageIdx := 1

		// Create a slice of all matches
		matches := newPagePattern.FindAllString(s, -1)

		// Replace each match one by one
		for _, match := range matches {
			replacement := fmt.Sprintf(`<br /><hr id="%d"/>`, pageIdx+1)
			s = strings.Replace(s, match, replacement, 1)
			pageIdx++
		}
	}

	// Replace newlines with HTML line breaks
	s = strings.ReplaceAll(s, "\n", "<br />")

	return HTML(s)
}

// getTemplateFunctions returns a map of custom template functions for use in HTML templates
func getTemplateFunctions() map[string]any {
	return map[string]any{
		"parseEmojis": func(s string) HTML {
			return parseEmojis(s)
		},
		"parsePixivRedirect": func(s string) HTML {
			return parsePixivRedirect(s)
		},
		"escapeString": func(s string) string {
			return escapeString(s)
		},
		"unescapeHTMLString": func(s string) string {
			return unescapeHTMLString(s)
		},
		"isEmphasize": func(s string) bool {
			switch s {
			case "R-18", "R-18G":
				return true
			}

			return false
		},
		"parseTime": func(date time.Time) string {
			return parseTime(date)
		},
		"parseTimeCustomFormat": func(date time.Time, format string) string {
			return parseTimeCustomFormat(date, format)
		},
		"naturalTime": func(date time.Time) string {
			return naturalTime(date)
		},
		"relativeTime": func(date time.Time) RelativeTimeData {
			return relativeTime(date)
		},
		"prettyNumber": func(n int) string {
			return prettyNumber(n)
		},
		"createPaginator": func(base, ending string, current_page, max_page, page_margin, dropdown_offset int) PaginationData {
			paginationData, err := createPaginator(base, ending, current_page, max_page, page_margin, dropdown_offset)
			if err != nil {
				fmt.Printf("Error creating paginator: %v", err)
				// Return an empty PaginationData in case of error
				return PaginationData{}
			}
			return paginationData
		},
		"parseNovelContent": func(s string) HTML {
			return ParseNovelContent(s)
		},
		"getNovelGenre": getNovelGenre,
		"floor": func(i float64) int {
			return int(math.Floor(i))
		},
		"ordinalNumeral":  ordinalNumeral,
		"unfinishedQuery": unfinishedQuery,
		"replaceQuery":    replaceQuery,
		"isFirstPathPart": isFirstPathPart,
		"isLastPathPart":  isLastPathPart,
		"IsFullPath":      isFullPath,
		"FormatWorkIDs":   formatWorkIDs,
	}
}
