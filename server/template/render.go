// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"reflect"
	"strings"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"codeberg.org/vnpower/pixivfe/v2/server/utils"
	"github.com/CloudyKit/jet/v6"
	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/html"
)

var (
	views             *jet.Set // global variable, yes.
	generateLinkToken LinkTokenGenerator
)

type LinkTokenGenerator func(*http.Request) (string, string, error)

func Init(disableCache bool, assetsLocation string, tokenGenerator LinkTokenGenerator) {
	if disableCache {
		views = jet.NewSet(
			newLocalizedFSLoader(assetsLocation),
			jet.InDevelopmentMode(), // disable cache
		)
	} else {
		views = jet.NewSet(
			newLocalizedFSLoader(assetsLocation),
		)
	}

	for fnName, fn := range getTemplateFunctions() {
		views.AddGlobal(fnName, fn)
	}

	generateLinkToken = tokenGenerator
}

func RenderHTML[T any](w http.ResponseWriter, r *http.Request, data T) error {
	return RenderWithContentType(w, r, "text/html; charset=utf-8", data)
}

func RenderWithContentType[T any](w http.ResponseWriter, r *http.Request, contentType string, data T) error {
	// Render and get ETag
	content, etag, serverTiming, err := Render(getTemplatingVariables(r), data)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", contentType)

	// Set strong ETag
	// NOTE: the ngx_brotli nginx module appears to weaken strong ETags (even though gzipping is fine)
	w.Header().Set("ETag", `"`+etag+`"`)

	// This is a conservative fallback; the proper cache-control
	// directives should be set in the route handler
	if w.Header().Get("Cache-Control") == "" {
		if session.GetUserToken(r) != "" {
			w.Header().Set("Cache-Control", "no-store")
		} else {
			w.Header().Set("Cache-Control", "private, max-age=60")
		}
	}

	// Write Server-Timing header
	w.Header().Add("Server-Timing", serverTiming)

	// Set Vary: Cookie to prevent user preferences from affecting the shared cache [1]
	w.Header().Set("Vary", "Cookie")

	// [1]: negatively affects HTTP cache hit rate, but we don't have the option of
	// client-side hydration via JS nor ESI so these will have to do

	w.WriteHeader(requestcontext.FromRequest(r).StatusCode)

	_, err = w.Write(content)

	return err
}

func Render[T any](variables jet.VarMap, data T) ([]byte, string, string, error) {
	templateName, found := strings.CutPrefix(reflect.TypeFor[T]().Name(), "Data_")
	if !found {
		log.Panicf("struct name does not start with 'Data_': %s", templateName)
	}

	// Determine template path based on if it's a partial
	templatePath := templateName + ".jet.html"
	if strings.HasSuffix(templateName, "Partial") {
		templatePath = "partials/" + templatePath
	}

	template, err := views.GetTemplate(templatePath)
	if err != nil {
		return nil, "", "", err
	}

	_, err = views.Parse(templatePath, template.String())
	if err != nil {
		return nil, "", "", err
	}

	buf := &bytes.Buffer{}

	renderStart := time.Now()
	// Create minifier
	m := minify.New()
	m.AddFunc("text/html", html.Minify)
	minWriter := m.Writer("text/html", buf)

	// Execute template directly to minifier writer
	if err := template.Execute(minWriter, variables, data); err != nil {
		return nil, "", "", err
	}

	renderDuration := time.Since(renderStart).Milliseconds()

	minifyStart := time.Now()
	// Close the minifier writer to flush any remaining content
	if err := minWriter.Close(); err != nil {
		return nil, "", "", err
	}

	minifyDuration := time.Since(minifyStart).Milliseconds()

	// Get the final bytes
	minifiedBytes := buf.Bytes()
	etag := GenerateETag(minifiedBytes)

	// Create Server-Timing value
	serverTiming := fmt.Sprintf(
		"template-render;dur=%d;desc=\"Template render\", html-minify;dur=%d;desc=\"HTML minify\"",
		renderDuration,
		minifyDuration,
	)

	return minifiedBytes, etag, serverTiming, nil
}

func getTemplatingVariables(r *http.Request) jet.VarMap {
	// Pass in values that we want to be available to all pages here
	token := session.GetUserToken(r)
	baseURL := utils.Origin(r)
	currentPath := r.URL.Path
	currentPathWithParams := r.URL.RequestURI()
	revision := config.GlobalConfig.Instance.Revision
	fullURL := r.URL.Scheme + r.Host + currentPath

	cookieList := map[string]string{}
	cookieListOrdered := []struct {
		k string
		v string
	}{}

	for _, name := range session.AllCookieNames {
		value := session.GetCookie(r, name)
		cookieList[string(name)] = value
		cookieListOrdered = append(cookieListOrdered, struct {
			k string
			v string
		}{string(name), value})
	}

	queries := make(map[string]string)

	for k, v := range r.URL.Query() {
		queries[k] = v[0]
	}

	// Allow checking whether initiated by an HTMX request or not within templates
	isHtmxRequest := r.Header.Get("HX-Request") == "true"

	// Parse HX-Current-URL to get proper current path for async requests
	var htmxCurrentPath string

	if htmxCurrentURL := r.Header.Get("HX-Current-URL"); htmxCurrentURL != "" {
		if parsedURL, err := url.Parse(htmxCurrentURL); err == nil {
			htmxCurrentPath = parsedURL.Path
		}
	}

	var (
		linkToken          string
		linkTokenIntegrity string
	)

	limiterEnabled := config.GlobalConfig.Limiter.Enabled
	if limiterEnabled {
		// FIXME: handle the error here
		if generateLinkToken != nil {
			linkToken, linkTokenIntegrity, _ = generateLinkToken(r)
		}
	}

	return jet.VarMap{}.
		Set("BaseURL", baseURL).
		Set("CurrentPath", currentPath).
		Set("CurrentPathWithParams", currentPathWithParams).
		Set("HtmxCurrentPath", htmxCurrentPath).
		// Set("DomainName", domainName).
		Set("FullURL", fullURL).
		Set("LoggedIn", token != "").
		Set("Queries", queries).
		Set("CookieList", cookieList).
		Set("CookieListOrdered", cookieListOrdered).
		Set("Revision", revision).
		Set("IsHtmxRequest", isHtmxRequest).
		Set("LimiterEnabled", limiterEnabled).
		Set("LinkToken", linkToken).
		Set("LinkTokenIntegrity", linkTokenIntegrity)
}
