// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package template

import (
	"encoding/base64"
	"encoding/binary"

	"github.com/zeebo/xxh3"
)

// GenerateETag creates a strong ETag from content bytes
func GenerateETag(content []byte) string {
	hash := xxh3.Hash(content)
	hashBytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(hashBytes, uint64(hash))
	return base64.RawURLEncoding.EncodeToString(hashBytes)
}
