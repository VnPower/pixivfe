// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/core"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
	"codeberg.org/vnpower/pixivfe/v2/server/utils"
	"golang.org/x/sync/errgroup"
)

func TagPage(w http.ResponseWriter, r *http.Request) error {
	param := GetPathVar(r, "name", GetQueryParam(r, "name"))
	name, err := url.PathUnescape(param)
	if err != nil {
		return err
	}

	page := GetQueryParam(r, "page", "1")
	pageInt, err := strconv.Atoi(page)
	if err != nil {
		return err
	}

	queries := core.ArtworkSearchSettings{
		Name:     name,
		Category: GetQueryParam(r, "category", "artworks"),
		Order:    GetQueryParam(r, "order", "date_d"),
		Mode:     GetQueryParam(r, "mode", "safe"),
		Ratio:    GetQueryParam(r, "ratio", ""),
		Wlt:      GetQueryParam(r, "wlt", ""),
		Wgt:      GetQueryParam(r, "wgt", ""),
		Hlt:      GetQueryParam(r, "hlt", ""),
		Hgt:      GetQueryParam(r, "hgt", ""),
		Tool:     GetQueryParam(r, "tool", ""),
		Scd:      GetQueryParam(r, "scd", ""),
		Ecd:      GetQueryParam(r, "ecd", ""),
		Page:     page,
	}

	var (
		tag    core.TagSearchResult
		result *core.ArtworkSearchResult
	)

	g, _ := errgroup.WithContext(r.Context())

	// Fetch tag data and search results concurrently
	g.Go(func() error {
		t, err := core.GetTagData(r, name)
		if err != nil {
			return err
		}
		tag = t

		// Fetch cover artwork after tag data is available
		id := tag.Metadata.ID.String()
		if id != "" {
			var illust core.Illust

			if err := core.GetBasicArtwork(r, id, &illust); err != nil {
				return err
			}

			tag.CoverArtwork = illust
		}

		// PreloadImage(w, tag.Metadata.ImageMaster)
		PreloadImage(w, tag.CoverArtwork.Thumbnails.Webp_1200)

		if config.GlobalConfig.Response.EarlyHintsResponsesEnabled {
			w.WriteHeader(http.StatusEarlyHints)
		}

		return nil
	})

	g.Go(func() error {
		res, err := core.GetSearch(r, queries)
		if err != nil {
			return err
		}
		result = res
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	urlc := template.PartialURL{
		Path:  "/tags",
		Query: queries.ReturnMap(),
	}

	if session.GetUserToken(r) != "" {
		w.Header().Set("Cache-Control", "private, max-age=60")
	} else {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
			int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
			int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))
	}

	return template.RenderHTML(w, r, Data_tag{
		Title:                "Results for " + name,
		Tag:                  tag,
		Data:                 *result,
		QueriesC:             urlc,
		TrueTag:              param,
		Page:                 pageInt,
		ActiveCategory:       queries.Category,
		ActiveOrder:          queries.Order,
		ActiveMode:           queries.Mode,
		ActiveRatio:          queries.Ratio,
		ActiveSearchMode:     GetQueryParam(r, "smode", ""),
		PopularSearchEnabled: config.GlobalConfig.Feature.PopularSearchEnabled,
	})
}

func AdvancedTagPost(w http.ResponseWriter, r *http.Request) error {
	return utils.RedirectTo(w, r, "/tags",
		map[string]string{
			"name":     GetQueryParam(r, "name", r.FormValue("name")),
			"category": GetQueryParam(r, "category", "artworks"),
			"order":    GetQueryParam(r, "order", "date_d"),
			"mode":     GetQueryParam(r, "mode", "safe"),
			"ratio":    GetQueryParam(r, "ratio"),
			"page":     GetQueryParam(r, "page", "1"),
			"wlt":      GetQueryParam(r, "wlt", r.FormValue("wlt")),
			"wgt":      GetQueryParam(r, "wgt", r.FormValue("wgt")),
			"hlt":      GetQueryParam(r, "hlt", r.FormValue("hlt")),
			"hgt":      GetQueryParam(r, "hgt", r.FormValue("hgt")),
			"tool":     GetQueryParam(r, "tool", r.FormValue("tool")),
			"scd":      GetQueryParam(r, "scd", r.FormValue("scd")),
			"ecd":      GetQueryParam(r, "ecd", r.FormValue("ecd")),
		})
}
