// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

func LoginPage(w http.ResponseWriter, r *http.Request) error {
	// Never cache the login page to be safe
	w.Header().Set("Cache-Control", "no-store")

	return template.RenderHTML(w, r, Data_login{
		Title: "Sign in",
	})
}
