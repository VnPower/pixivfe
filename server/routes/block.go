package routes

import (
	"log"
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

// BlockPage writes a block page using the provided reason, status code, and template.
func BlockPage(w http.ResponseWriter, r *http.Request, reason string, statusCode int) {
	// Update the per-request status code.
	reqCtx := requestcontext.FromRequest(r)
	reqCtx.StatusCode = statusCode

	// Get the path the user was attempting to access.
	path := r.URL.Path

	w.Header().Set("Cache-Control", "no-store")

	// Render the HTML error page.
	if renderErr := template.RenderHTML(w, r, Data_block{
		Title:      "Blocked",
		Reason:     reason,
		StatusCode: statusCode,
		Path:       path,
	}); renderErr != nil {
		log.Printf("Failed to render block page: %v", renderErr)
	}
}
