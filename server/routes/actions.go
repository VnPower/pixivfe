// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"bytes"
	"fmt"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"

	"github.com/goccy/go-json"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/core"
	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/i18n"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
	"codeberg.org/vnpower/pixivfe/v2/server/utils"
)

const (
	returnPathFormat    string = "return_path"
	bookmarkCountFormat string = "bookmark_count"
	likeCountFormat     string = "like_count"
	artworkIDFormat     string = "artwork_id"
	bookmarkIDFormat    string = "bookmark_id"
	userIDFormat        string = "user_id"
	privateFormat       string = "private" // privacy setting for bookmarks and follows
)

// NOTE: is the csrf protection by the upstream Pixiv API itself good enough, or do we need to implement our own?

// When handling htmx requests, we pass all bookmark-related illust data
// to the opposite partial (add ↔ delete) to ensure state preservation
// during client-side swaps.
func AddBookmarkRoute(w http.ResponseWriter, r *http.Request) error {
	token := session.GetUserToken(r)
	csrf := session.GetCookie(r, session.Cookie_CSRF)
	returnPath := r.FormValue(returnPathFormat)
	noAuthReturnPath := returnPath
	loginReturnPath := returnPath
	strBookmarkCount := r.FormValue(bookmarkCountFormat)

	bookmarkCount, err := strconv.Atoi(strBookmarkCount)
	if err != nil {
		return i18n.Error("Invalid bookmark count.")
	}

	artworkID := GetPathVar(r, artworkIDFormat)
	if artworkID == "" {
		return i18n.Error("No ID provided.")
	}

	if token == "" || csrf == "" {
		return UnauthorizedPage(w, r, noAuthReturnPath, loginReturnPath)
	}

	privateVal := r.FormValue(privateFormat)
	isPrivate := privateVal == "true" || privateVal == "on"

	restrict := "0"
	if isPrivate {
		restrict = "1"
	}

	payload := fmt.Sprintf(`{
        "illust_id": "%s",
        "restrict": %s,
        "comment": "",
        "tags": []
    }`, artworkID, restrict)

	contentType := "application/json; charset=utf-8"

	apiURL := core.PostAddIllustBookmarkURL()

	cookies := map[string]string{
		"PHPSESSID": token,
	}

	rawResp, err := requests.PerformPOST(r.Context(), apiURL, payload, cookies, csrf, contentType, r.Header)
	if err != nil {
		return err
	}

	var resp core.AddIllustBookmarkResponse
	if err := json.Unmarshal(rawResp.Body, &resp); err != nil {
		return err
	}

	bookmarkID := resp.Body.LastBookmarkID
	userID := strings.Split(token, "_")
	userURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s/illusts/bookmarks", userID[0])
	artworkURL := fmt.Sprintf("https://www.pixiv.net/ajax/illust/%s", artworkID)
	urlsToInvalidate := []string{userURL, artworkURL}

	invalidatedCount, invalidatedURLs := requests.InvalidateURLs(urlsToInvalidate)
	audit.GlobalAuditor.Logger.Infow("Invalidated cache entries",
		"userId", userID[0],
		"invalidatedCount", invalidatedCount,
		"requestedURLs", urlsToInvalidate,
		"invalidatedURLs", invalidatedURLs,
	)

	isHtmx := r.Header.Get("HX-Request") == "true"
	if isHtmx {
		return template.RenderHTML(w, r, Data_deleteBookmarkPartial{
			Illust: core.Illust{
				ID: artworkID,
				BookmarkData: &core.BookmarkData{
					ID: bookmarkID,
				},
				Bookmarks: bookmarkCount + 1,
			},
		})
	}

	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}

func DeleteBookmarkRoute(w http.ResponseWriter, r *http.Request) error {
	token := session.GetUserToken(r)
	csrf := session.GetCookie(r, session.Cookie_CSRF)
	returnPath := r.FormValue(returnPathFormat)
	noAuthReturnPath := returnPath
	loginReturnPath := returnPath
	strBookmarkCount := r.FormValue(bookmarkCountFormat)

	bookmarkCount, err := strconv.Atoi(strBookmarkCount)
	if err != nil {
		return i18n.Error("Invalid bookmark count.")
	}

	bookmarkID := GetPathVar(r, bookmarkIDFormat)
	if bookmarkID == "" {
		return i18n.Error("No ID provided.")
	}

	if token == "" || csrf == "" {
		return UnauthorizedPage(w, r, noAuthReturnPath, loginReturnPath)
	}

	payload := fmt.Sprintf("bookmark_id=%s", bookmarkID)
	contentType := "application/x-www-form-urlencoded; charset=utf-8"
	apiURL := core.PostDeleteIllustBookmarkURL()
	cookies := map[string]string{
		"PHPSESSID": token,
	}

	_, err = requests.PerformPOST(r.Context(), apiURL, payload, cookies, csrf, contentType, r.Header)
	if err != nil {
		return err
	}

	artworkID := r.FormValue(artworkIDFormat)
	userID := strings.Split(token, "_")
	userURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s/illusts/bookmarks", userID[0])
	artworkURL := fmt.Sprintf("https://www.pixiv.net/ajax/illust/%s", artworkID)
	urlsToInvalidate := []string{userURL, artworkURL}

	invalidatedCount, invalidatedURLs := requests.InvalidateURLs(urlsToInvalidate)
	audit.GlobalAuditor.Logger.Infow("Invalidated cache entries",
		"userId", userID[0],
		"invalidatedCount", invalidatedCount,
		"requestedURLs", urlsToInvalidate,
		"invalidatedURLs", invalidatedURLs,
	)

	isHtmx := r.Header.Get("HX-Request") == "true"
	if isHtmx {
		return template.RenderHTML(w, r, Data_addBookmarkPartial{
			Illust: core.Illust{
				ID:        artworkID,
				Bookmarks: bookmarkCount - 1,
			},
		})
	}

	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}

func LikeRoute(w http.ResponseWriter, r *http.Request) error {
	token := session.GetUserToken(r)
	csrf := session.GetCookie(r, session.Cookie_CSRF)
	returnPath := r.FormValue(returnPathFormat)
	noAuthReturnPath := returnPath
	loginReturnPath := returnPath
	strLikeCount := r.FormValue(likeCountFormat)

	likeCount, err := strconv.Atoi(strLikeCount)
	if err != nil {
		return i18n.Error("Invalid bookmark count.")
	}

	artworkID := GetPathVar(r, artworkIDFormat)
	if artworkID == "" {
		return i18n.Error("No ID provided.")
	}

	if token == "" || csrf == "" {
		return UnauthorizedPage(w, r, noAuthReturnPath, loginReturnPath)
	}

	payload := fmt.Sprintf(`{"illust_id": "%s"}`, artworkID)
	contentType := "application/json; charset=utf-8"
	apiURL := core.PostIllustLikeURL()
	cookies := map[string]string{
		"PHPSESSID": token,
	}

	_, err = requests.PerformPOST(r.Context(), apiURL, payload, cookies, csrf, contentType, r.Header)
	if err != nil {
		return err
	}

	userID := strings.Split(token, "_")
	artworkURL := fmt.Sprintf("https://www.pixiv.net/ajax/illust/%s", artworkID)
	urlsToInvalidate := []string{artworkURL}

	invalidatedCount, invalidatedURLs := requests.InvalidateURLs(urlsToInvalidate)
	audit.GlobalAuditor.Logger.Infow("Invalidated cache entries",
		"userId", userID[0],
		"invalidatedCount", invalidatedCount,
		"requestedURLs", urlsToInvalidate,
		"invalidatedURLs", invalidatedURLs,
	)

	isHtmx := r.Header.Get("HX-Request") == "true"
	if isHtmx {
		return template.RenderHTML(w, r, Data_unlikePartial{
			Illust: core.Illust{
				Likes: likeCount + 1,
			},
		})
	}

	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}

/*
NOTE: we're using the mobile API for FollowRoute and UnfollowRoute since it's an actual AJAX API
			instead of some weird php thing for the usual desktop routes (/bookmark_add.php and /rpc_group_setting.php)

			the desktop routes return HTML for the pixiv SPA when they feel like it and don't return helpful responses
			when you send a request that doesn't perfectly meet their specifications, making troubleshooting a nightmare

			for comparison, the mobile API worked first try without any issues

			interestingly enough, replicating the requests for the desktop routes via cURL worked fine but a Go implementation
			just refused to work
*/

func FollowRoute(w http.ResponseWriter, r *http.Request) error {
	token := session.GetUserToken(r)
	csrf := session.GetCookie(r, session.Cookie_CSRF)
	returnPath := r.FormValue(returnPathFormat)
	noAuthReturnPath := returnPath
	loginReturnPath := returnPath

	followUserID := r.FormValue(userIDFormat)
	if followUserID == "" {
		return i18n.Error("No user ID provided.")
	}

	privateVal := r.FormValue(privateFormat)
	isPrivate := privateVal == "true" || privateVal == "on"

	restrict := "0"
	if isPrivate {
		restrict = "1"
	}

	if token == "" || csrf == "" {
		return UnauthorizedPage(w, r, noAuthReturnPath, loginReturnPath)
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	writer.WriteField("mode", "add_bookmark_user")
	writer.WriteField("restrict", restrict)
	writer.WriteField("user_id", followUserID)
	writer.Close()

	fields := map[string]string{
		"mode":     "add_bookmark_user",
		"restrict": restrict,
		"user_id":  followUserID,
	}
	url := core.PostTouchAPI()
	cookies := map[string]string{
		"PHPSESSID": token,
	}

	rawResp, err := requests.PerformPOST(r.Context(), url, fields, cookies, csrf, "", r.Header)
	if err != nil {
		return err
	}

	// Parse response
	var resp core.FollowResponse
	if err := json.Unmarshal(rawResp.Body, &resp); err != nil {
		return fmt.Errorf("failed to parse follow response: %w", err)
	}

	// Check if the follow operation was successful
	if !resp.IsSucceed {
		return fmt.Errorf("failed to follow user")
	}

	userID := strings.Split(token, "_")
	followerFollowingListURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s/following", userID[0])
	followerProfileURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s?full=", userID[0])
	followedProfileURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s?full=", followUserID)
	urlsToInvalidate := []string{followerFollowingListURL, followerProfileURL, followedProfileURL}

	invalidatedCount, invalidatedURLs := requests.InvalidateURLs(urlsToInvalidate)
	audit.GlobalAuditor.Logger.Infow("Invalidated cache entries",
		"userId", userID[0],
		"followUserId", followUserID,
		"invalidatedCount", invalidatedCount,
		"requestedURLs", urlsToInvalidate,
		"invalidatedURLs", invalidatedURLs,
	)

	isHtmx := r.Header.Get("HX-Request") == "true"
	if isHtmx {
		returnPath := r.FormValue(returnPathFormat)
		if returnPath == "" {
			returnPath = r.URL.Path
		}

		return template.RenderHTML(w, r, Data_deleteFollowPartial{
			User: core.User{
				ID:         followUserID,
				IsFollowed: true,
			},
		})
	}

	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}

func UnfollowRoute(w http.ResponseWriter, r *http.Request) error {
	token := session.GetUserToken(r)
	csrf := session.GetCookie(r, session.Cookie_CSRF)
	returnPath := r.FormValue(returnPathFormat)
	noAuthReturnPath := returnPath
	loginReturnPath := returnPath

	followUserID := GetQueryParam(r, userIDFormat)
	if followUserID == "" {
		return i18n.Error("No user ID provided.")
	}

	if token == "" || csrf == "" {
		return UnauthorizedPage(w, r, noAuthReturnPath, loginReturnPath)
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	writer.WriteField("mode", "delete_bookmark_user")
	writer.WriteField("user_id", followUserID)
	writer.Close()

	fields := map[string]string{
		"mode":    "delete_bookmark_user",
		"user_id": followUserID,
	}
	url := core.PostTouchAPI()
	cookies := map[string]string{
		"PHPSESSID": token,
	}

	rawResp, err := requests.PerformPOST(r.Context(), url, fields, cookies, csrf, "", r.Header)
	if err != nil {
		return err
	}

	// Parse response
	var resp core.FollowResponse
	if err := json.Unmarshal(rawResp.Body, &resp); err != nil {
		return fmt.Errorf("failed to parse follow response: %w", err)
	}

	// Check if the follow operation was successful
	if !resp.IsSucceed {
		return fmt.Errorf("failed to unfollow user")
	}

	userID := strings.Split(token, "_")
	followerFollowingListURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s/following", userID[0])
	followerProfileURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s?full=", userID[0])
	followedProfileURL := fmt.Sprintf("https://www.pixiv.net/ajax/user/%s?full=", followUserID)
	urlsToInvalidate := []string{followerFollowingListURL, followerProfileURL, followedProfileURL}

	invalidatedCount, invalidatedURLs := requests.InvalidateURLs(urlsToInvalidate)
	audit.GlobalAuditor.Logger.Infow("Invalidated cache entries",
		"userId", userID[0],
		"followUserId", followUserID,
		"invalidatedCount", invalidatedCount,
		"requestedURLs", urlsToInvalidate,
		"invalidatedURLs", invalidatedURLs,
	)

	isHtmx := r.Header.Get("HX-Request") == "true"
	if isHtmx {
		returnPath := r.FormValue(returnPathFormat)
		if returnPath == "" {
			returnPath = r.URL.Path
		}

		return template.RenderHTML(w, r, Data_addFollowPartial{
			User: core.User{
				ID:         followUserID,
				IsFollowed: false,
			},
		})
	}

	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}
