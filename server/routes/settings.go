// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"bufio"
	"io"
	"net/http"
	"regexp"
	"slices"
	"strings"

	"github.com/goccy/go-json"

	"codeberg.org/vnpower/pixivfe/v2/audit"
	"codeberg.org/vnpower/pixivfe/v2/config"

	"codeberg.org/vnpower/pixivfe/v2/core"

	"codeberg.org/vnpower/pixivfe/v2/core/requests"
	"codeberg.org/vnpower/pixivfe/v2/i18n"
	"codeberg.org/vnpower/pixivfe/v2/server/proxychecker"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
	"codeberg.org/vnpower/pixivfe/v2/server/utils"
)

var (
	r_csrf = regexp.MustCompile(`"token":"([0-9a-f]+)"`)
	r_p_ab = regexp.MustCompile(`'p_ab_d_id': "(\d+)"`)
)

func setToken(w http.ResponseWriter, r *http.Request) (string, error) {
	token := r.FormValue("token")
	if token == "" {
		return "", i18n.Error("You submitted an empty/invalid form.")
	}

	cookies := map[string]string{
		"PHPSESSID": token,
	}

	// Request API route to check if token is valid
	url := core.GetNewestFromFollowingURL("illust", "all", "1")
	_, err := requests.FetchJSONBodyField(r.Context(), url, cookies, r.Header)
	if err != nil {
		return "", i18n.Error("Cannot authorize with supplied token. (API returned not OK)")
	}

	// Request artwork page to extract csrf token from body
	// THE TEST URL IS NSFW!
	rawResp, err := requests.PerformGET(r.Context(), "https://www.pixiv.net/en/artworks/115365120", cookies, r.Header)
	if err != nil {
		return "", err
	}

	if rawResp.StatusCode != http.StatusOK {
		return "", i18n.Error("Cannot authorize with supplied token. (Page returned not OK)")
	}

	// Extract CSRF token
	csrfMatches := r_csrf.FindStringSubmatch(string(rawResp.Body))
	if len(csrfMatches) < 2 {
		return "", i18n.Error("Unable to extract CSRF token from response.")
	}
	csrf := csrfMatches[1]

	// Extract personal ID
	personalIDMatches := r_p_ab.FindStringSubmatch(string(rawResp.Body))
	if len(personalIDMatches) < 2 {
		return "", i18n.Error("Unable to extract a personal ID from response.")
	}
	personalID := personalIDMatches[1]

	if personalID == "" {
		return "", i18n.Error("Cannot authorize with supplied token. (Invalid personal ID)")
	}

	// Get user ID from token
	// The left part of the token is the member ID
	userID := strings.Split(token, "_")

	// Get user information
	userInfoURL := core.GetUserInformationURL(userID[0], "1")
	rawResp2, err := requests.FetchJSONBodyField(r.Context(), userInfoURL, cookies, r.Header)
	if err != nil {
		return "", err
	}

	rawResp2, err = session.ProxyContentURLs(r, rawResp2)
	if err != nil {
		return "", err
	}

	// Parse user info
	var userInfo struct {
		UserID   string `json:"userId"`
		Username string `json:"name"`
		Avatar   string `json:"imageBig"`
	}

	if err := json.Unmarshal(rawResp2, &userInfo); err != nil {
		return "", err
	}

	// Set all cookies
	session.SetCookie(w, session.Cookie_Token, token)
	session.SetCookie(w, session.Cookie_CSRF, csrf)
	session.SetCookie(w, session.Cookie_P_AB, personalID)
	session.SetCookie(w, session.Cookie_Username, userInfo.Username)
	session.SetCookie(w, session.Cookie_UserID, userInfo.UserID)
	session.SetCookie(w, session.Cookie_UserAvatar, userInfo.Avatar)

	return i18n.Sprintf("Successfully logged in."), nil
}

func setImageServer(w http.ResponseWriter, r *http.Request) (string, error) {
	customProxy := r.FormValue("custom-image-proxy")
	selectedProxy := r.FormValue("image-proxy")

	switch {
	case customProxy != "":
		return handleCustomProxy(w, customProxy)
	case selectedProxy != "":
		return handleSelectedProxy(w, selectedProxy)
	default:
		session.ClearCookie(w, session.Cookie_ImageProxy)
		return i18n.Sprintf("Image proxy server cleared. Using default proxy."), nil
	}
}

func handleCustomProxy(w http.ResponseWriter, customProxy string) (string, error) {
	var proxyURL string

	if customProxy == config.BuiltInImageProxyPath {
		proxyURL = config.BuiltInImageProxyPath
	} else {
		parsedURL, err := utils.ValidateURL(customProxy, "Custom image proxy")
		if err != nil {
			return "", err
		}
		proxyURL = parsedURL.String()
	}

	session.SetCookie(w, session.Cookie_ImageProxy, proxyURL)
	return i18n.Sprintf("Image proxy server set successfully to: %s", proxyURL), nil
}

func handleSelectedProxy(w http.ResponseWriter, selectedProxy string) (string, error) {
	proxyURL := selectedProxy

	session.SetCookie(w, session.Cookie_ImageProxy, proxyURL)
	return i18n.Sprintf("Image proxy server set successfully to: %s", proxyURL), nil
}

func setVisualEffects(w http.ResponseWriter, r *http.Request) (string, error) {
	visualEffects := r.FormValue("visual-effects")

	if visualEffects == "on" {
		session.SetCookie(w, session.Cookie_VisualEffectsEnabled, "true")
		return i18n.Sprintf("Visual effects preference updated successfully."), nil
	} else if visualEffects == "" {
		session.SetCookie(w, session.Cookie_VisualEffectsEnabled, "false")
		return i18n.Sprintf("Visual effects preference updated successfully."), nil
	}

	return "", i18n.Error("Invalid visual effects preference.")
}

func setTimeZone(w http.ResponseWriter, r *http.Request) (string, error) {
	timeZone := r.FormValue("time-zone")

	// Validate timezone
	_, err := session.ValidateTimezone(timeZone)
	if err != nil {
		return "", i18n.Error("Invalid timezone specified.")
	}

	if timeZone == "" {
		session.ClearCookie(w, session.Cookie_TZ)
		return i18n.Sprintf("Timezone reset to UTC."), nil
	}

	session.SetCookie(w, session.Cookie_TZ, timeZone)
	return i18n.Sprintf("Timezone updated successfully to %s.", timeZone), nil
}

func setNovelFontType(w http.ResponseWriter, r *http.Request) (string, error) {
	fontType := r.FormValue("font-type")
	if fontType != "" {
		session.SetCookie(w, session.Cookie_NovelFontType, fontType)
		return i18n.Sprintf("Novel font type updated successfully."), nil
	}

	return "", i18n.Error("Invalid font type.")
}

func setNovelViewMode(w http.ResponseWriter, r *http.Request) (string, error) {
	viewMode := r.FormValue("view-mode")
	if viewMode == "1" || viewMode == "2" || viewMode == "" {
		session.SetCookie(w, session.Cookie_NovelViewMode, viewMode)
		return i18n.Sprintf("Novel view mode updated successfully."), nil
	}

	return "", i18n.Error("Invalid view mode.")
}

func setThumbnailToNewTab(w http.ResponseWriter, r *http.Request) (string, error) {
	ttnt := r.FormValue("ttnt")
	if ttnt == "_blank" {
		session.SetCookie(w, session.Cookie_ThumbnailToNewTab, ttnt)
		return i18n.Sprintf("Thumbnails will now open in a new tab."), nil
	}

	session.SetCookie(w, session.Cookie_ThumbnailToNewTab, "_self")
	return i18n.Sprintf("Thumbnails will now open in the same tab."), nil
}

func setArtworkPreview(w http.ResponseWriter, r *http.Request) (string, error) {
	value := r.FormValue("app")
	if value == "cover" || value == "button" || value == "" {
		session.SetCookie(w, session.Cookie_ArtworkPreview, value)
		return i18n.Sprintf("Artwork preview setting updated successfully."), nil
	}

	return "", i18n.Error("Invalid artwork preview setting.")
}

func setFilter(w http.ResponseWriter, r *http.Request) (string, error) {
	r18 := r.FormValue("filter-r18")
	r18g := r.FormValue("filter-r18g")
	censorAi := r.FormValue("censor-ai")
	hideAi := r.FormValue("filter-ai")

	session.SetCookie(w, session.Cookie_HideArtR18, r18)
	session.SetCookie(w, session.Cookie_HideArtR18G, r18g)
	session.SetCookie(w, session.Cookie_CensorArtAI, censorAi)
	session.SetCookie(w, session.Cookie_HideArtAI, hideAi)

	return i18n.Sprintf("Filter settings updated successfully."), nil
}

func setLogout(w http.ResponseWriter, _ *http.Request) (string, error) {
	// Clear-Site-Data header with wildcard to clear everything
	w.Header().Set("Clear-Site-Data", "*")

	// Cookie clearing as fallback
	session.ClearCookie(w, session.Cookie_Token)
	session.ClearCookie(w, session.Cookie_CSRF)
	session.ClearCookie(w, session.Cookie_P_AB)
	session.ClearCookie(w, session.Cookie_Username)
	session.ClearCookie(w, session.Cookie_UserID)
	session.ClearCookie(w, session.Cookie_UserAvatar)
	return i18n.Sprintf("Successfully logged out."), nil
}

func setCookie(w http.ResponseWriter, r *http.Request) (string, error) {
	key := r.FormValue("key")
	value := r.FormValue("value")

	for _, cookieName := range session.AllCookieNames {
		if string(cookieName) == key {
			session.SetCookie(w, cookieName, value)
			return i18n.Sprintf("Cookie %s set successfully.", key), nil
		}
	}
	return "", i18n.Errorf("Invalid Cookie Name: %s", key)
}

func clearCookie(w http.ResponseWriter, r *http.Request) (string, error) {
	key := r.FormValue("key")

	for _, cookieName := range session.AllCookieNames {
		if string(cookieName) == key {
			session.ClearCookie(w, cookieName)
			return i18n.Sprintf("Cookie %s cleared successfully.", key), nil
		}
	}

	return "", i18n.Errorf("Invalid Cookie Name: %s", key)
}

func setRawCookie(w http.ResponseWriter, r *http.Request) (string, error) {
	raw := r.FormValue("raw")
	reader := bufio.NewReader(strings.NewReader(raw))

	for {
		line, isPrefix, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		if isPrefix {
			return "", bufio.ErrBufferFull
		}

		if err != nil {
			return "", err
		}

		sub := strings.Split(string(line), "=")
		if len(sub) != 2 {
			continue
		}

		name := session.CookieName(sub[0])
		value := sub[1]

		if !slices.Contains(session.AllCookieNames, name) {
			continue
		}

		session.SetCookie(w, name, value)
	}
	return i18n.Sprintf("Raw settings applied successfully."), nil
}

func resetAll(w http.ResponseWriter, _ *http.Request) (string, error) {
	// Clear-Site-Data header with wildcard to clear everything
	w.Header().Set("Clear-Site-Data", "*")

	// Cookie clearing as fallback
	session.ClearAllCookies(w)
	return i18n.Sprintf("All preferences have been reset to default values."), nil
}

func SettingsPage(w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Cache-Control", "private, max-age=60")

	return template.RenderHTML(w, r, Data_settings{
		WorkingProxyList:   proxychecker.GetWorkingProxies(),
		ProxyList:          config.BuiltInImageProxyList,
		ProxyCheckEnabled:  config.GlobalConfig.ProxyChecker.Enabled,
		ProxyCheckInterval: config.GlobalConfig.ProxyChecker.Interval,
		DefaultProxyServer: config.GlobalConfig.ContentProxies.Image.String(),
	})
}

func handleAJAXResponse(w http.ResponseWriter, message string, err error) {
	w.Header().Set("Content-Type", "text/html")

	var (
		html       string
		statusCode int
	)

	if err != nil {
		statusCode = http.StatusBadRequest
		html = i18n.Sprintf(
			`<div id="form-htmx-target"></div>
			<div id="form-htmx-target" class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
			%s<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>`, err.Error())
	} else {
		statusCode = http.StatusOK
		html = i18n.Sprintf(
			`<div id="form-htmx-target"></div>
			<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
			%s<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>`, message)
	}

	w.WriteHeader(statusCode)

	if _, werr := w.Write([]byte(html)); werr != nil {
		audit.GlobalAuditor.Logger.Error("Error writing response: %v", werr)
	}
}

func SettingsPost(w http.ResponseWriter, r *http.Request) error {
	actionType := GetPathVar(r, "type") // TODO: this should just be called "action"

	// Map setting types to their respective functions.
	actions := map[string]func(http.ResponseWriter, *http.Request) (string, error){
		"imageServer":       setImageServer,
		"token":             setToken,
		"logout":            setLogout,
		"timeZone":          setTimeZone,
		"reset-all":         resetAll,
		"novelFontType":     setNovelFontType,
		"thumbnailToNewTab": setThumbnailToNewTab,
		"novelViewMode":     setNovelViewMode,
		"artworkPreview":    setArtworkPreview,
		"filter":            setFilter,
		"visualEffects":     setVisualEffects,
		"set-cookie":        setCookie,
		"clear-cookie":      clearCookie,
		"raw":               setRawCookie,
	}

	var (
		message string
		err     error
	)

	if action, ok := actions[actionType]; ok {
		message, err = action(w, r)
	} else {
		err = i18n.Error("No such setting is available.")
	}

	if r.Header.Get("HX-Request") == "true" {
		handleAJAXResponse(w, message, err)
		return nil
	}

	if err != nil {
		return err
	}

	returnPath := r.FormValue("returnPath")
	if returnPath != "" {
		http.Redirect(w, r, returnPath, http.StatusSeeOther)
	} else {
		utils.RedirectToWhenceYouCame(w, r)
	}

	return nil
}
