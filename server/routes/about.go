// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"fmt"
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

func AboutPage(w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
		int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
		int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))

	return template.RenderHTML(w, r, Data_about{
		Time:           config.GlobalConfig.Instance.StartingTime,
		Version:        config.GlobalConfig.Instance.Version,
		DomainName:     r.Host, // Used to template in the actual domain name serving PixivFE
		RepoURL:        config.GlobalConfig.Instance.RepoURL,
		Revision:       config.GlobalConfig.Instance.Revision,
		RevisionHash:   config.GlobalConfig.Instance.RevisionHash, // Used for the link to the source code repo
		ImageProxy:     config.GlobalConfig.ContentProxies.Image.String(),
		AcceptLanguage: config.GlobalConfig.Request.AcceptLanguage,
	})
}
