// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package routes

import (
	"fmt"
	"net/http"
	"strconv"

	"codeberg.org/vnpower/pixivfe/v2/config"
	"codeberg.org/vnpower/pixivfe/v2/core"
	"codeberg.org/vnpower/pixivfe/v2/server/session"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

func PixivisionHomePage(w http.ResponseWriter, r *http.Request) error {
	page := GetQueryParam(r, "p", "1")
	data, err := core.PixivisionGetHomepage(r, page, "en")
	if err != nil {
		return err
	}

	pageint, err := strconv.Atoi(page)
	if err != nil {
		return err
	}

	if session.GetUserToken(r) != "" {
		w.Header().Set("Cache-Control", "private, max-age=60")
	} else {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
			int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
			int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))
	}

	return template.RenderHTML(w, r, Data_pixivisionIndex{
		Data: data,
		Page: pageint,
	})
}

func PixivisionArticlePage(w http.ResponseWriter, r *http.Request) error {
	id := GetPathVar(r, "id")
	data, err := core.PixivisionGetArticle(r, id, "en")
	if err != nil {
		return err
	}

	if session.GetUserToken(r) != "" {
		w.Header().Set("Cache-Control", "private, max-age=60")
	} else {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
			int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
			int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))
	}

	return template.RenderHTML(w, r, Data_pixivisionArticle{
		Article: data,
	})
}

func PixivisionCategoryPage(w http.ResponseWriter, r *http.Request) error {
	page := GetQueryParam(r, "p", "1")
	id := GetPathVar(r, "id")
	data, err := core.PixivisionGetCategory(r, id, page, "en")
	if err != nil {
		return err
	}

	pageint, err := strconv.Atoi(page)
	if err != nil {
		return err
	}

	if session.GetUserToken(r) != "" {
		w.Header().Set("Cache-Control", "private, max-age=60")
	} else {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
			int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
			int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))
	}

	return template.RenderHTML(w, r, Data_pixivisionCategory{
		Category: data,
		Page:     pageint,
		ID:       id,
	})
}

func PixivisionTagPage(w http.ResponseWriter, r *http.Request) error {
	id := GetPathVar(r, "id")
	page := GetQueryParam(r, "p", "1")

	data, err := core.PixivisionGetTag(r, id, page, "en")
	if err != nil {
		return err
	}

	pageint, err := strconv.Atoi(page)
	if err != nil {
		return err
	}

	if session.GetUserToken(r) != "" {
		w.Header().Set("Cache-Control", "private, max-age=60")
	} else {
		w.Header().Set("Cache-Control", fmt.Sprintf("public, max-age=%d, stale-while-revalidate=%d",
			int(config.GlobalConfig.HTTPCache.MaxAge.Seconds()),
			int(config.GlobalConfig.HTTPCache.StaleWhileRevalidate.Seconds())))
	}

	return template.RenderHTML(w, r, Data_pixivisionTag{
		Tag:  data,
		Page: pageint,
		ID:   id,
	})
}
