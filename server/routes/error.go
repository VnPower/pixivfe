package routes

import (
	"log"
	"net/http"

	"codeberg.org/vnpower/pixivfe/v2/server/requestcontext"
	"codeberg.org/vnpower/pixivfe/v2/server/template"
)

// ErrorPage writes an error page using the provided error, status code, and template.
//
// It updates the request context status code so that the template has access to it, and logs
// any error encountered while rendering the error page.
func ErrorPage(w http.ResponseWriter, r *http.Request, err error, statusCode int) {
	// Update the per-request status code.
	reqCtx := requestcontext.FromRequest(r)
	reqCtx.StatusCode = statusCode

	w.Header().Set("Cache-Control", "no-store")

	// Render the HTML error page.
	if renderErr := template.RenderHTML(w, r, Data_error{
		Title:      "Error",
		Error:      err,
		StatusCode: statusCode,
	}); renderErr != nil {
		log.Printf("Failed to render error page: %v", renderErr)
	}
}
