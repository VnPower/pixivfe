// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package utils

import (
	"crypto/tls"
	"net/http"
)

// HttpClient is a pre-configured http.Client.
//
// It serves as a base HTTP client used across different packages.
var HttpClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{
			ClientSessionCache: tls.NewLRUClientSessionCache(20),
		},
		Proxy:               http.ProxyFromEnvironment,
		MaxIdleConns:        0,
		MaxIdleConnsPerHost: 20,
		WriteBufferSize:     32 * 1024, // 32KB
		ReadBufferSize:      32 * 1024, // 32KB
	},
}

// // Http3Client is a pre-configured http.Client with HTTP/3 capabilities.
// var Http3Client = &http.Client{
// 	Transport: &http3.Transport{
// 		QUICConfig: &quic.Config{
// 		},
// 	},
// }
