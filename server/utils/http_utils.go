// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package utils

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/goccy/go-json"
)

// SendString writes a plain text response to the provided http.ResponseWriter.
// It sets the content type to "text/plain" and returns any error encountered during writing.
func SendString(w http.ResponseWriter, text string) error {
	w.Header().Set("content-type", "text/plain")
	_, err := w.Write([]byte(text))
	return err
}

// SendJson encodes the provided data as JSON and writes it to the http.ResponseWriter.
// It automatically sets the appropriate content type header.
func SendJson(w http.ResponseWriter, data any) {
	json.NewEncoder(w).Encode(data)
}

// RedirectTo performs a redirect to the specified path with optional query parameters.
// It uses HTTP status 303 (See Other) for the redirect.
func RedirectTo(w http.ResponseWriter, r *http.Request, path string, query_params map[string]string) error {
	query := url.Values{}
	for k, v := range query_params {
		query.Add(k, v)
	}
	http.Redirect(w, r, path+"?"+query.Encode(), http.StatusSeeOther)
	return nil
}

// RedirectToWhenceYouCame redirects the user back to the referring page if it's from the same origin.
//
// This helps prevent open redirects by checking the referrer against the current origin.
// If the referrer is not from the same origin, it responds with a 200 OK status.
//
// NOTE: deprecated in favor of providing a returnPath parameter to allow
// instances to set "Referrer-Policy: no-referrer"
func RedirectToWhenceYouCame(w http.ResponseWriter, r *http.Request) {
	referrer := r.Referer()
	if strings.HasPrefix(referrer, Origin(r)) {
		// // Append a query parameter indicating a redirect (if not already present).
		// referrerURL, err := url.Parse(referrer)
		// if err == nil && referrerURL.Query().Get("redirected") == "" {
		// 	q := referrerURL.Query()
		// 	q.Set("redirected", "true")
		// 	referrerURL.RawQuery = q.Encode()
		// 	referrer = referrerURL.String()
		// }
		http.Redirect(w, r, referrer, http.StatusSeeOther)
	} else {
		w.WriteHeader(200)
	}
}

// Origin returns the origin (scheme + host) from an HTTP request.
// The scheme is determined by first checking the X-Forwarded-Proto header,
// then the TLS connection status, defaulting to "http" if neither is set.
// The result is returned in the format "scheme://host".
func Origin(r *http.Request) string {
	scheme := "http"

	// Check X-Forwarded-Proto header first
	if proto := r.Header.Get("X-Forwarded-Proto"); proto != "" {
		scheme = proto
	} else if r.TLS != nil {
		scheme = "https"
	}

	return scheme + "://" + r.Host
}
