// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

/*
Helpers
*/
package router

import (
	"bytes"
	"io"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"codeberg.org/vnpower/pixivfe/v2/server/routes"

	"codeberg.org/vnpower/pixivfe/v2/server/template"
	"github.com/gorilla/mux"
)

// staticFileHandler wraps http.ServeContent and adds ETags.
func staticFileHandler(root http.FileSystem) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Set appropriate cache-control on CSS to prevent clients from using outdated versions
		if strings.HasSuffix(r.URL.Path, ".css") {
			w.Header().Set("Cache-Control", "max-age=0, stale-while-revalidate=86400, must-revalidate")
		} else {
			w.Header().Set("Cache-Control", "public, max-age=31536000")
		}

		file, err := root.Open(r.URL.Path)
		if err != nil {
			http.NotFound(w, r)

			return
		}
		defer file.Close()

		content, err := io.ReadAll(file)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		prefix := filepath.Base(r.URL.Path)
		etag := template.GenerateETag(content)
		w.Header().Set("ETag", etag)

		// Empty time so that ServeContent only uses ETag
		http.ServeContent(w, r, prefix, time.Time{}, bytes.NewReader(content))
	})
}

// handleStripPrefix is a utility function that combines path prefix matching with
// stripping the prefix from the request URL before passing it to the handler.
func handleStripPrefix(router *mux.Router, pathPrefix string, handler http.Handler) *mux.Route {
	return router.PathPrefix(pathPrefix).Handler(http.StripPrefix(pathPrefix, handler))
}

// hasTrailingSlash is a helper function to check for trailing slashes.
func hasTrailingSlash(r *http.Request, _ *mux.RouteMatch) bool {
	return r.URL.Path != "/" && strings.HasSuffix(r.URL.Path, "/")
}

// removeTrailingSlash is a helper function to remove trailing slash and redirect.
func removeTrailingSlash(w http.ResponseWriter, r *http.Request) {
	url := r.URL

	if len(url.Path) > 1 {
		url.Path = url.Path[:len(url.Path)-1]
	}

	// iacore: i think this won't have open redirect vuln
	http.Redirect(w, r, url.String(), http.StatusPermanentRedirect)
}

// legacyRedirect is a helper function to redirect requests to
// a target path while preserving the specified query parameter.
func legacyRedirect(targetPath string, preservedParam string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, targetPath+routes.GetQueryParam(r, preservedParam), http.StatusPermanentRedirect)
	}
}
