// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package proxychecker

import (
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"codeberg.org/vnpower/pixivfe/v2/audit"
)

func TestUpdateAndGetWorkingProxies(t *testing.T) {
	t.Parallel()

	audit.NewTestingLogger(t)

	newProxies := []string{"http://proxy1.invalid", "http://proxy2.invalid"}
	updateWorkingProxies(newProxies)

	result := GetWorkingProxies()
	if len(result) != len(newProxies) {
		t.Errorf("Expected %d proxies, got %d", len(newProxies), len(result))
	}

	for i, proxy := range result {
		if proxy != newProxies[i] {
			t.Errorf("Expected proxy %s, got %s", newProxies[i], proxy)
		}
	}

	// Test concurrent access
	var waitGroup sync.WaitGroup
	for range 10 {
		waitGroup.Add(1)

		go func() {
			defer waitGroup.Done()

			_ = GetWorkingProxies()
		}()
	}

	waitGroup.Wait()
}

func TestTestProxy(t *testing.T) {
	t.Parallel()

	audit.NewTestingLogger(t)

	// Create a temporary backup of the original testImageCRC32,
	// and override with a value we know our test server will produce (all zeros for empty body)
	originalCRC32 := testImageCRC32
	testImageCRC32 = "00000000"

	// Restore the original testImageCRC32 after the test
	defer func() { testImageCRC32 = originalCRC32 }()

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	defer server.Close()

	isWorking, resp := testProxy(t.Context(), server.URL)
	if resp != nil {
		defer resp.Body.Close() // Close the response body to avoid resource leaks
	}

	if !isWorking {
		t.Errorf("Expected proxy to be working")
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, resp.StatusCode)
	}

	//nolint:bodyclose // no need to close resp.Body here as resp is expected to be nil
	isWorking, resp = testProxy(t.Context(), "http://nonexistentproxy.invalid")
	if isWorking {
		t.Errorf("Expected proxy to be not working")
	}

	if resp != nil {
		t.Errorf("Expected nil response for non-working proxy")
	}

	// Test with a server that returns an error status
	errorServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	}))
	defer errorServer.Close()

	isWorking, resp = testProxy(t.Context(), errorServer.URL)
	if resp != nil {
		defer resp.Body.Close()
	}

	if isWorking {
		t.Errorf("Expected proxy to be not working due to error status")
	}

	if resp == nil || resp.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected error status response")
	}
}
