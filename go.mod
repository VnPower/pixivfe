module codeberg.org/vnpower/pixivfe/v2

go 1.24

toolchain go1.24.0

// for go-jnode
replace encoding/json => github.com/goccy/go-json v0.10.3

require (
	github.com/CloudyKit/jet/v6 v6.3.1
	github.com/PuerkitoBio/goquery v1.10.2
	github.com/andybalholm/cascadia v1.3.3
	github.com/go-faker/faker/v4 v4.6.0
	github.com/goccy/go-json v0.10.5
	github.com/gorilla/mux v1.8.1
	github.com/oklog/ulid/v2 v2.1.0
	github.com/sethvargo/go-envconfig v1.1.1
	github.com/sethvargo/go-limiter v1.0.0
	github.com/soluble-ai/go-jnode v0.1.11
	github.com/tdewolff/minify/v2 v2.21.3
	github.com/tidwall/gjson v1.18.0
	github.com/timandy/routine v1.1.4
	github.com/yargevad/filepathx v1.0.0
	github.com/zeebo/xxh3 v1.0.2
	go.uber.org/zap v1.27.0
	golang.org/x/net v0.37.0
	golang.org/x/sync v0.12.0
	golang.org/x/time v0.11.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/klauspost/cpuid/v2 v2.2.10 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/rogpeppe/go-internal v1.13.1 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
	github.com/tdewolff/parse/v2 v2.7.20 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/zeebo/assert v1.3.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	golang.org/x/text v0.23.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
