// Copyright 2023 - 2025, VnPower and the PixivFE contributors
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"slices"
	"testing"
)

func TestGetRandomUserAgent(t *testing.T) {
	// Test that the function returns a non-empty string
	ua := GetRandomUserAgent()
	if ua == "" {
		t.Error("GetRandomUserAgent returned an empty string")
	}

	// Test that the returned user agent is in the list
	if !slices.Contains(BuiltinUserAgentList, ua) {
		t.Error("GetRandomUserAgent returned a user agent not in BuiltinUserAgentList")
	}

	// Test that BuiltinUserAgentList is not empty
	if len(BuiltinUserAgentList) == 0 {
		t.Error("BuiltinUserAgentList is empty")
	}
}
